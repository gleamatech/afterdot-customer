import React, { Fragment } from 'react';
import { Button } from 'antd';
import Header from 'containers/Header';
import Footer from 'containers/Footer';
import { useRouter } from 'next/router';

import './style.scss';
import HeaderAuth from 'containers/HeaderAuth';

const Layout = ({ children }) => {
  const router = useRouter();
  const routerAuth = [
    '/login',
    '/signup',
    '/forgot-password',
    '/signup/introduction/[id]',
    '/signup/risk/[id]',
    '/signup/risk-tolerance'
  ];
  return (
    <Fragment>
      {routerAuth.includes(router.pathname) ? <HeaderAuth /> : <Header />}

      <div
        className={`layout-content-wrapper ${
          routerAuth.includes(router.pathname) && 'none-padding'
        }`}
      >
        {children}
      </div>
      {!routerAuth.includes(router.pathname) && <Footer />}
    </Fragment>
  );
};

export default Layout;
