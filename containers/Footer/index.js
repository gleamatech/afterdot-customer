import React from "react";
import { Row, Col } from "antd";
import Link from "next/link";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedinIn,
  FaGoogle,
} from "react-icons/fa";

import images from "config/images";

import "./style.scss";

const listRowTop = [
  {
    title: "About Blooom",
    arrContent: [
      "About",
      "What We Do",
      "Why We Do It",
      "How It Works",
      "Manifesto",
      "Pricing",
    ],
  },
  {
    title: "Learn More",
    arrContent: [
      "Blooom Booost",
      "Education Center",
      "FAQ",
      "Meet the Team",
      "Fee Calculator",
      "Allocation Calculator",
      "Diversification Calculator",
      "Spending Calculator",
    ],
  },
  {
    title: "RESOURCES",
    arrContent: [
      "Blog",
      "In the News",
      "Press Center",
      "Affiliate",
      "Careers",
      "Contact",
    ],
  },
  {
    title: "LEGAL",
    arrContent: [
      "Relationship Summary",
      "Terms & Conditions",
      "Privacy Policy",
      "Accessibility",
      "Security",
      "General Promo Guidelines",
      "Refer-A-Friend Terms",
      "All Legal",
    ],
  },
];

const listRowBottom = [
  {
    title: "LET'S DO IT",
    arrContent: ["Start Now", "Log In"],
  },
  {
    title: "ADDRESS",
    arrContent: ["5325 W 115th Pl", "Leawood, KS 66211"],
  },
  {
    title: "HOURS",
    arrContent: ["Monday - Friday", "9:00 am - 4:00 pm"],
  },
  {
    title: "SOCIAL",
  },
];

const Footer = () => {
  const renderListItem = (listItem) => {
    return listItem.map((item, index) => {
      const path ='/'
      return (
        <Col xs={24} sm={12} className="footer-content__list-item" key={index}>
          <h2 className="footer-content__item__title">{item.title}</h2>
          <div className="footer-content__item__link-wrapper">
            {item.title !== "SOCIAL" ? (
              item.arrContent.map((it, idx) => (
                <div key={idx}>
                  <Link href={path}>
                  <a>{it}</a>
                  </Link>
                </div>
              ))
            ) : (
              <div className="footer-content__social">
                <a>
                  <FaFacebookF />
                </a>
                <a>
                  <FaTwitter />
                </a>
                <a>
                  <FaInstagram />
                </a>
                <a>
                  <FaGoogle />
                </a>
                <a>
                  <FaLinkedinIn />
                </a>
              </div>
            )}
          </div>
        </Col>
      );
    });
  };

  return (
    <div className="footer-wrapper">
      <div className="footer-top">
        <div className="container">
          <div className="footer-content">
            <div className="footer-content__logo">
              <img src={images.logo} />
            </div>
            <Row gutter={[{ xs: 0, sm: 50 }, 16]}>
              {renderListItem(listRowTop)}
            </Row>
            <Row gutter={[{ xs: 0, sm: 50 }, 16]}>
              {renderListItem(listRowBottom)}
            </Row>
            <p className="footer-content__text">
              The information is provided for discussion purposes only and
              should not be considered as investment advice. Blooom is limited
              to the funds available in your retirement plan. There is no
              guarantee blooom can or will reduce your fund expenses. Blooom
              relies on the ability to receive updated account and transaction
              information. We do not guarantee we will be able to provide
              account monitoring and withdrawal alerts to all accounts.
            </p>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <Row gutter={[50,0]}>
            <Col className="footer-bottom__wrapper">
                <div className="footer-bottom__left">
                  © 2021 blooom. All Rights Reserved.
                </div>
                <div className="footer-bottom__right">
                  <Link href="/">
                    <a>Web Design in Kansas City</a>
                  </Link>{" "}
                  <span>by</span>{" "}
                  <Link href="/">
                    <a>Lifted Logic</a>
                  </Link>
                </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default Footer;
