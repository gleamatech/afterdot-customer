import React, { useEffect, useState } from 'react';
import { motion } from 'framer-motion';
import { Turn as Hamburger } from 'hamburger-react';
import { Form, Input, Button } from 'antd';
import { FaChevronLeft, FaSignOutAlt } from 'react-icons/fa';
import Link from 'next/link';
import { useRouter } from 'next/router';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getItemStorage, removeItemStorage } from 'common/functions';
import { setUserRedux } from 'controller/redux/action/storageActions';

const HeaderAuth = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [token, setToken] = useState(null);
  const user = useSelector((state) => state.userRedux);

  const handleToggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleCloseMenu = () => {
    setIsOpen(false);
  };

  const preRouter = () => () => {
    router.back();
  };

  const handleSignOut = () => {
    dispatch(setUserRedux(null));
  };

  const itemAnimate = {
    open: {
      opacity: 1,
      height: 'auto',
      x: 0,
      transition: {
        duration: 0.5
      },
      display: 'block'
    },
    close: {
      opacity: 0,
      height: 0,
      x: 1000,
      transition: {
        duration: 0.5
      },
      transitionEnd: {
        display: 'none'
      }
    }
  };

  useEffect(() => {
    setToken(getItemStorage('JWT_TOKEN'));
    if (!user) {
      removeItemStorage('JWT_TOKEN');
    }
  }, [user]);

  return (
    <header className='header-auth'>
      <nav className='header-auth__header'>
        <span onClick={preRouter()}>
          <div className='header-auth__header-back'>
            <FaChevronLeft />
            <p>Back</p>
          </div>
        </span>

        <Link href='/'>
          <a className='header-auth__header-logo'>
            <img src='/images/login-logo.svg' alt='' />
          </a>
        </Link>
        <div className='header-auth__navbar-mobile'>
          <Hamburger size={24} onToggle={handleToggleMenu} toggled={isOpen} />
        </div>
      </nav>
      <motion.div
        initial='close'
        animate={!isOpen ? 'close' : 'open'}
        variants={itemAnimate}
        className='header-auth__sidebar-mobile'
      >
        <nav className='header-auth__header'>
          <span></span>
          <Link href='/'>
            <a className='header-auth__header-logo'>
              <img src='/images/login-logo.svg' alt='' />
            </a>
          </Link>
          <div>
            <Hamburger size={24} onToggle={handleCloseMenu} toggled={isOpen} />
          </div>
        </nav>
        {token && user ? (
          <>
            <div className='header-auth__sidebar-menu'>
              <Link href='/about/financial-planning'>
                <a>About Blooom</a>
              </Link>
              <Link href='/about/financial-planning'>
                <a>How Blooom Works</a>
              </Link>
              <Link href='/blog'>
                <a>Blog</a>
              </Link>
              <div className='sign-out' onClick={handleSignOut}>
                <p>Sign Out</p>
                <FaSignOutAlt />
              </div>
            </div>
          </>
        ) : (
          <div className='header-auth__sidebar-menu'>
            <Link href='/about/financial-planning'>
              <a>About Blooom</a>
            </Link>
            <Link href='/about/financial-planning'>
              <a>How Blooom Works</a>
            </Link>
            <Link href='/blog'>
              <a>Blog</a>
            </Link>
            <Link href='/login'>
              <a>Log In</a>
            </Link>
          </div>
        )}
      </motion.div>
    </header>
  );
};

export default HeaderAuth;
