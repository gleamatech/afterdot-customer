import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Menu, Dropdown, Button, Space } from 'antd';
import { motion } from 'framer-motion';
import { FaChevronDown } from 'react-icons/fa';
import { Turn as Hamburger } from 'hamburger-react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';

import NewButton from 'components/Common/NewButton';
import images from 'config/images';

import './style.scss';
import { getItemStorage, removeItemStorage } from 'common/functions';
import { setUserLogOut, setUserRedux } from 'controller/redux/action/storageActions';

const Header = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [isOpen, setIsOpen] = useState(false);
  const [keyActive, setKeyActive] = useState('');
  const [token, setToken] = useState(null);
  const user = useSelector((state) => state.userRedux);
  const MenuAbout = (
    <Menu>
      <Menu.Item>
        <Link href='/about/financial-planning'>
          <a>About blooom</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href='/about/robo-investing'>
          <a>What we do</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href='/about/best-robo-advisor'>
          <a>Why we do it</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href='/about/fiduciary-financial-advisor-near-me'>
          <a>Meet the team</a>
        </Link>
      </Menu.Item>
    </Menu>
  );

  const MenuFAQ = (
    <Menu>
      <Menu.Item>
        <Link href='/'>
          <a>How blooom works</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href='/'>
          <a>Pricing</a>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link href='/'>
          <a>FAQ</a>
        </Link>
      </Menu.Item>
    </Menu>
  );

  const itemAnimate = {
    open: {
      opacity: 1,
      height: 'auto',
      transition: {
        duration: 0.3
      },
      display: 'block'
    },
    close: {
      opacity: 0,
      height: 0,
      transition: {
        duration: 0.3
      },
      transitionEnd: {
        display: 'none'
      }
    }
  };

  const handleToggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleCloseMenu = () => {
    setIsOpen(false);
    setKeyActive(false)
  };

  const handleToggleDropdown = (value) => () => {
    if (value === keyActive) {
      setKeyActive('');
    } else setKeyActive(value);
  };

  const handleSignOut = () => {
    dispatch(setUserRedux(null));
  };

  useEffect(() => {
    setToken(getItemStorage('JWT_TOKEN'));
    if (!user) {
      removeItemStorage('JWT_TOKEN');
    }
  }, [user]);

  useEffect(() => {
    setKeyActive('');
    router.events.on('routeChangeComplete', handleCloseMenu);
    return () => {
      router.events.off('routeChangeComplete', handleCloseMenu);
    };
  }, [router.events]);

  return (
    <div className='header-wrapper'>
      <div className='container'>
        <div className='header'>
          <div className='header__logo'>
            <Link href='/'>
              <a>
                <img src={images.logo} />
              </a>
            </Link>
          </div>
          <div className='header__navbar'>
            <div
              className={`header__navbar__item ${
                keyActive === 'about' && 'header__navbar__item--active'
              }`}
            >
              <a onClick={handleToggleDropdown('about')}>
                <span>About</span>
                <FaChevronDown />
              </a>
              {keyActive === 'about' && (
                <ul className='header__dropdown'>
                  <li>
                    <Link href='/about/financial-planning'>
                      <p>About blooom</p>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/robo-investing'>
                      <p>Pricing</p>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/best-robo-advisor'>
                      <p>Why we do it</p>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/fiduciary-financial-advisor-near-me'>
                      <p>Meet the team</p>
                    </Link>
                  </li>
                </ul>
              )}
            </div>
            <div
              className={`header__navbar__item ${
                keyActive === 'faq' && 'header__navbar__item--active'
              }`}
            >
              <a onClick={handleToggleDropdown('faq')}>
                <span>How it works</span>
                <FaChevronDown />
              </a>
              {keyActive === 'faq' && (
                <ul className='header__dropdown'>
                  <li>
                    <Link href='/about/financial-planning'>
                      <p>How blooom works</p>
                    </Link>
                  </li>
                  <li>
                    <Link href='/robo-advisor-fees'>
                      <p>Pricing</p>
                    </Link>
                  </li>
                  <li>
                    <Link href='/faq'>
                      <p>FAQ</p>
                    </Link>
                  </li>
                </ul>
              )}
            </div>

            <Link href='/blog'>
              <a className='header__navbar__item'>Blog</a>
            </Link>
            {token && user ? (
              <div
                className={`header__navbar__item ${
                  keyActive === 'user' && 'header__navbar__item--active'
                }`}
              >
                <a onClick={handleToggleDropdown('user')}>
                  <span>{user.name}</span>
                  <FaChevronDown />
                </a>
                {keyActive === 'user' && (
                  <ul className='header__dropdown header__dropdown-user '>
                    <li onClick={handleSignOut}>
                      <p>Sign Out</p>
                    </li>
                  </ul>
                )}
              </div>
            ) : (
              <>
                <Link href='/login'>
                  <NewButton text='LOG IN' type='primary' className='header__navbar__item' />
                </Link>
                <Link href='/signup/introduction/1'>
                  <NewButton text='START NOW' type='second' className='header__navbar__item' />
                </Link>
              </>
            )}
          </div>
          <div className='header__navbar--mobile'>
            <Hamburger size={24} onToggle={handleToggleMenu} toggled={isOpen} />
          </div>
        </div>
        <motion.div
          initial='close'
          animate={!isOpen ? 'close' : 'open'}
          variants={itemAnimate}
          className='header__dropdown--mobile'
        >
          <div className='header__navbar__dropdown'>
            <div
              className={`header__navbar__item ${
                keyActive === 'about' && 'header__navbar__item--active'
              }`}
            >
              <a onClick={handleToggleDropdown('about')}>
                About <FaChevronDown />
              </a>
              {keyActive === 'about' && (
                <ul>
                  <li>
                    <Link href='/about/financial-planning'>
                      <a>About blooom</a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/robo-investing'>
                      <a>What we do</a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/best-robo-advisor'>
                      <a>Why we do it</a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/about/fiduciary-financial-advisor-near-me'>
                      <a>Meet the team</a>
                    </Link>
                  </li>
                </ul>
              )}
            </div>
            <div
              className={`header__navbar__item ${
                keyActive === 'faq' && 'header__navbar__item--active'
              }`}
            >
              <a onClick={handleToggleDropdown('faq')}>
                How it works <FaChevronDown />
              </a>
              {keyActive === 'faq' && (
                <ul>
                  <li>
                    <Link href='/'>
                      <a>How blooom works</a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/'>
                      <a>Pricing</a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/faq'>
                      <a>FAQ</a>
                    </Link>
                  </li>
                </ul>
              )}
            </div>
            <div className='header__navbar__item'>
              <Link href='/blog'>
                <a>Blog</a>
              </Link>
            </div>

            {token && user ? (
              <div
                className={`header__navbar__item ${
                  keyActive === 'user' && 'header__navbar__item--active'
                }`}
              >
                <a onClick={handleToggleDropdown('user')}>
                  <span>{user.name}</span>
                  <FaChevronDown />
                </a>
                {keyActive === 'user' && (
                  <ul>
                    <li onClick={handleSignOut}>
                      <p>Sign Out</p>
                    </li>
                  </ul>
                )}
              </div>
            ) : (
              <>
                <div className='header__navbar__item'>
                  <Link href='/login'>
                    <NewButton text='LOG IN' type='primary' />
                  </Link>
                </div>
                <div className='header__navbar__item'>
                  <Link href='/signup/introduction/1'>
                    <NewButton text='START NOW' type='second' />
                  </Link>
                </div>
              </>
            )}
          </div>
        </motion.div>
      </div>
    </div>
  );
};

export default Header;
