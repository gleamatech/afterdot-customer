import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { FaChevronLeft } from 'react-icons/fa';
import parse from 'html-react-parser';
import { BaseAPI } from '../../../config/BaseAPI';
import './style.scss';
import { useRouter } from 'next/router';
import { setBlogByTag } from 'controller/redux/action/pageActions';

const URL_SLUG = '/api/posts/slug/';

const DetailScreen = () => {
  const router = useRouter();
  const querySlug = router.query.slug;

  const dispatch = useDispatch();
  const post = useSelector((state) => state.pageRedux.blogPage.blogDetails);

  const slug = post ? post.slug : querySlug;
  const [infoDetails, setInfoDetails] = useState([]);
  const fetchApi = async () => {
    try {
      const response = await BaseAPI.get(`${URL_SLUG}${slug}`);
      if (!response.status) return;
      setInfoDetails(response.data.doc);
    } catch (error) {}
  };

  const handlePostsByTag = (item) => async () => {
    await dispatch(setBlogByTag(item));
    router.push(`/blog/tag/${item}`);
  };

  useEffect(() => {
    fetchApi();
  }, []);

  return (
    <main className='detailscreen'>
      <div className='detailscreen__banner'>
        <img src={infoDetails.image} alt='' />
        <h2 className='detailscreen__banner-title'>
          <div className='container'>
            <div className='detailscreen__content-wrapper'>{infoDetails.title}</div>
          </div>
        </h2>
      </div>
      <div className='detailscreen__article'>
        <div className='container'>
          <div className='detailscreen__content-wrapper'>
            <Link href='/blog'>
              <div className='detailscreen__article-back'>
                <div className='back-link-icon'>
                  <FaChevronLeft />
                </div>
                <p>Back to Blog</p>
              </div>
            </Link>

            <div className='detailscreen__article-content'>{parse(`${infoDetails.content}`)}</div>
            <div className='detailscreen__article__tags'>
              {infoDetails.tags &&
                infoDetails.tags.map((item, index) => (
                  <div
                    className='detailscreen__article__tags-item'
                    value={item}
                    onClick={handlePostsByTag(item)}
                    key={index}
                  >
                    {item}
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default DetailScreen;
