import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaChevronDown } from 'react-icons/fa';
import { List, Pagination, Form, Input, Spin } from 'antd';
import { BaseAPI } from '../../config/BaseAPI';
import InfiniteScroll from 'react-infinite-scroll-component';
import { setBlogDetail } from '../../controller/redux/action/pageActions';
import './style.scss';
import Link from 'next/link';
import { useRouter } from 'next/router';

const URL_POSTS = '/api/posts';
const URL_CATEGORIES = '/api/categories';
const URL_FILTER_CATEGORY = '/api/posts/category';
const URL_SEARCH = '/api/posts/search';
const URL_POST_TAG = '/api/posts/tag';
const PAGE_NUMBER = 1;

const BlogScreen = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [dataPosts, setDataPosts] = useState([]);
  const [listCategory, setListCategory] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [idCategory, setIdCategory] = useState('ALL');
  const [valueSearch, setValueSearch] = useState(null);
  const [totalResult, setTotalResult] = useState();
  const [keyFilter, setKeyFilter] = useState(''); // 1/CATEGORY, 2/SEARCH, 3/ALL
  const blogByTag = router.query.name;
  const renderListCategory = () => {
    return listCategory.map((item, index) => (
      <option key={index} value={item.id}>
        {item.name.en}
      </option>
    ));
  };

  const fetchBlogByTag = async () => {
    if (blogByTag) {
      const params = {
        tag: [`${blogByTag}`]
      };
      try {
        const response = await BaseAPI.post(URL_POST_TAG, params);
        if (!response.status) return;
        setDataPosts(response.data.docs);
      } catch (error) {}
    }
  };

  const fetchApi = async () => {
    const params = {
      limit: 12,
      page: page
    };

    switch (keyFilter) {
      case 'CATEGORY':
        try {
          const categories = await BaseAPI.post(
            `${URL_FILTER_CATEGORY}?limit=${params.limit}&page=${params.page}`,
            { category: idCategory }
          );
          if (!categories.status) return;
          setTotalResult(categories.total);
          setDataPosts([...dataPosts, ...categories.data.docs]);
        } catch (error) {}

        break;

      case 'SEARCH':
        try {
          const search = await BaseAPI.post(
            `${URL_SEARCH}?limit=${params.limit}&page=${params.page}`,
            { keyword: valueSearch }
          );
          if (!search.status) return;
          setTotalResult(search.total);
          setDataPosts([...dataPosts, ...search.data.docs]);
        } catch (error) {}

        break;

      default:
        // CASE ALL
        try {
          const response = await BaseAPI.get(URL_POSTS, { params });
          setTotalResult(response.total);
          setDataPosts([...dataPosts, ...response.data.docs]);
        } catch (error) {}

        break;
    }
  };

  const fetchCategories = async () => {
    const categories = await BaseAPI.get(URL_CATEGORIES);
    setListCategory(categories.data.docs);
  };

  const onChangeCategory = (event) => {
    // If select change -> Reset Data Post, Page Current, value input search
    if (idCategory !== event.target.value) {
      setDataPosts([]);
      setPage(1);
      setValueSearch(null);
    }

    // Set key filter let's call FetchAPI
    if (event.target.value === 'all') {
      setKeyFilter('ALL');
    } else {
      setKeyFilter('CATEGORY');
    }
    // save IdCategory let's call API item by id
    setIdCategory(event.target.value);
  };

  const onSearch = async (value) => {
    // If input search change -> Reset Data Post, Id Current, value input search
    if (valueSearch !== value.search) {
      setDataPosts([]);
      setIdCategory(null);
      setPage(1);
    }
    if (value.search === '') {
      setKeyFilter('ALL');
    } else {
      setKeyFilter('SEARCH');
    }
    // Save input value let's call API item by keyword
    setValueSearch(value.search);
  };

  useEffect(() => {
    // Fetch API for select element category
    fetchCategories();
  }, []);

  useEffect(() => {
    fetchBlogByTag();
  }, [blogByTag]);

  useEffect(() => {
    if (!blogByTag) {
      fetchApi();
    }
  }, [page, idCategory, valueSearch]);

  return (
    <main className='blogscreen'>
      <div className='container'>
        <div className='blogscreen__content-wrapper'>
          <div className='blogscreen__content-text'>
            {valueSearch ? (
              <h2>Search results for {valueSearch}</h2>
            ) : blogByTag ? (
              <h2>Blog By Tag</h2>
            ) : (
              <Link href='/blog'>
                <h2>Blog</h2>
              </Link>
            )}
          </div>
          {router.pathname === '/blog' && (
            <div className='blogscreen__content__filter'>
              <h1>CHOOSE A CATEGORY</h1>
              <div className='blogscreen__content__filter-group'>
                <div className='blogscreen__content__filter-select'>
                  <select name='' id='' onChange={onChangeCategory}>
                    <option value='all'>All</option>
                    {renderListCategory()}
                  </select>
                  <div className='select-icon'>
                    <FaChevronDown />
                  </div>
                </div>
                <Form onFinish={onSearch} initialValues={{ search: '' }}>
                  <div className='blogscreen__content__filter-search'>
                    <Form.Item name='search'>
                      <Input placeholder='Search Posts' />
                    </Form.Item>
                    <Form.Item>
                      <button type='submit'>SEARCH</button>
                    </Form.Item>
                  </div>
                </Form>
              </div>
            </div>
          )}
          <InfiniteScroll
            dataLength={dataPosts.length}
            next={() => setPage(page + 1)}
            hasMore={totalResult > dataPosts.length ? true : false}
            style={{
              width: '100%',
              overflow: 'hidden'
            }}
            scrollThreshold={'30%'}
          >
            <div className='blogscreen__content__post-list'>
              {dataPosts.map((item, index) => (
                <Link href={`/blog/${item.slug}`} key={index}>
                  <div
                    className='blogscreen__content__post-item'
                    onClick={() => dispatch(setBlogDetail(item))}
                  >
                    <div className='blog-image'>
                      <img src={item.image} alt='' />
                    </div>
                    <div className='blogscreen__content__post-item-content'>
                      <h1>{item.title}</h1>
                      <p>{item.desc}</p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
            {!dataPosts.length && <div>Sorry, no results were found.</div>}
          </InfiniteScroll>
        </div>
      </div>
    </main>
  );
};

export default BlogScreen;
