import React, { useState } from 'react';
import { motion } from 'framer-motion';
import NewButton from 'components/Common/NewButton';
import { Turn as Hamburger } from 'hamburger-react';
import { Form, Input } from 'antd';
import { FaFacebook, FaApple } from 'react-icons/fa';
import Link from 'next/link';
import './style.scss';
import { BaseAPI } from 'config/BaseAPI';
import { useRouter } from 'next/router';
import { setMessageAuth } from 'controller/redux/action/pageActions';
import { useDispatch } from 'react-redux';

const URL_FORGOT_PASSWORDS = '/api/users/forgotpassword';

const ForgotPasswordScreen = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [isFocus, setIsFocus] = useState(false);
  const [isValue, setIsValue] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleToggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleCloseMenu = () => {
    setIsOpen(false);
  };

  const itemAnimate = {
    open: {
      opacity: 1,
      height: 'auto',
      transition: {
        duration: 0.3
      },
      display: 'block'
    },
    close: {
      opacity: 0,
      height: 0,
      transition: {
        duration: 0.3
      },
      transitionEnd: {
        display: 'none'
      }
    }
  };

  const handleForgotPassword = async (values) => {
    setIsLoading(true);
    try {
      const { email } = values;
      const response = await BaseAPI.post(URL_FORGOT_PASSWORDS, { email: email });
      if (!response.status) return;
      dispatch(setMessageAuth({ actionsLoading: true, success: `${response.message} ${email}` }));
      router.push('/login');
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  return (
    <main className='forgot-password-screen'>
      <div className='container'>
        <div className={`forgot-password-screen__wrapper ${isOpen ? 'overlay-blur' : ''}`}>
          <div className='forgot-password-screen__form'>
            <h3>Forgot Your Password</h3>
            <p>
              Not a problem! Enter your email below, then check your inbox for password reset
              instructions.
            </p>
            <Form name='basic' onFinish={handleForgotPassword} initialValues={{ remember: true }}>
              <Form.Item
                name='email'
                label='Email'
                className='form-item'
                rules={[
                  { required: true, message: 'Require' },
                  { required: true, type: 'email', message: 'Must be a valid email address' }
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item>
                <NewButton text='Reset My Password' loading={isLoading} type='primary' />
              </Form.Item>
            </Form>
          </div>

          <div className='forgot-password-screen__directions'>
            <Link href='/signup'>
              <p>Sign up</p>
            </Link>
            <span></span>
            <Link href='/login'>
              <p>Sign in</p>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
};

export default ForgotPasswordScreen;
