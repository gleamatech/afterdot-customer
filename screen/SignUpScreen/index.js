import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Spin } from 'antd';
import Link from 'next/link';
import { FaFacebook, FaApple } from 'react-icons/fa';
import './style.scss';
import NewButton from 'components/Common/NewButton';
import { BaseAPI } from '../../config/BaseAPI';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { setUserRedux } from 'controller/redux/action/storageActions';
import { setItemStorage } from 'common/functions';
import get from 'lodash/get'
import { googleProvider, facebookProvider } from 'config/firebase/authMethod';
import socialMediaAuth from 'common/service';
import { KEY_STORE } from 'common/constans';

const URL_REGISTER = '/api/users/register';
const initialValues = {
  name: '',
  email: '',
  password: '',
  passwordConfirm: '',
  phoneNumber: ''
};

const SignUpScreen = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [messageSuccess, setMessageSuccess] = useState([]);
  const [messageError, setMessageError] = useState([]);
  // const isActiveIntroduction = useSelector((state) => state.pageRedux.signUp.activeIntroduction);
  const [isLoading, setIsLoading] = useState(false);
  const [isFocusName, setIsFocusName] = useState(false)
  const [isFocusEmail, setIsFocusEmail] = useState(false)
  const [isFocusPass, setIsFocusPass] = useState(false)
  const [isFocusConfirmPass, setIsFocusConfirmPass] = useState(false)

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    pass: '',
    confirmPass: ''
  })

  const onFinish = async (values) => {
    setIsLoading(true);
    const { name, email, password, passwordConfirm, phoneNumber } = values;
    const params = {
      name,
      email,
      password,
      passwordConfirm
    };
    try {
      const response = await BaseAPI.post(URL_REGISTER, params);
      if (response === undefined) {
        setMessageError('Please check your registration or account information already exists!');
      }
      if (response.status) {
        // Save token to local storage and login system
        const { token, user } = response;
        setItemStorage('JWT_TOKEN', token);
        dispatch(setUserRedux(user));
        router.push('/');
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
    }
  };

  const handleFocus =
    (type = "name", value) =>
    () => {
      if (type === "name") {
        if (get(formData, "name")) {
          setIsFocusName(true);
        } else setIsFocusName(value);
      } else if (type === "email") {
        if (get(formData, "email")) {
          setIsFocusEmail(true);
        } else setIsFocusEmail(value);
      } else if (type === "pass") {
        if (get(formData, "pass")) {
          setIsFocusPass(true);
        } else setIsFocusPass(value);
      } else {
        if (get(formData, "confirmPass")) {
          setIsFocusConfirmPass(true);
        } else setIsFocusConfirmPass(value);
      }
    };

    const handleChange = (e, name) => {
      const { value } = e.target
        setFormData({
          ...formData,
          [name]: value
        })
    }

    const onClickSocial = (provider) => async () => {
      const res = await socialMediaAuth(provider);
      if (res.status) {
        const user = res.data.getIdTokenResult().then(async (token) => {
          setIsLoading(true);
          const response = await BaseAPI.post("/api/users/loginWithSocial", {
            token: token.token,
          });
          if (response.status) {
            const { token, user } = response;
            setItemStorage(KEY_STORE.JWT_TOKEN, token);
            dispatch(setUserRedux({ token, user }));
            setIsLoading(false);
            router.push("/");
          }
        });
      }
    };

  return (
    <main className='signup-screen'>
      <div className='process-bar'>
        <div className='filter' style={{ width: '87.5%' }}></div>
      </div>
      <div className='container'>
        <div className='signup-screen__wrapper'>
          <h4>Let's set up your account...</h4>
          <p className='signup-error'>{messageError && messageError}</p>
          <Form onFinish={onFinish} initialValues={initialValues}>
            <Form.Item
              name='name'
              label='Name'
              className={`form-item ${isFocusName && 'form-item--isFocus'}`}
              rules={[{ required: true, message: 'Require' }]}
            >
              <Input value={formData.name} onChange={(e) => handleChange(e,'name')} onBlur={handleFocus('name', false)} onFocus={handleFocus('name', true)}/>
            </Form.Item>
            <Form.Item
              name='email'
              label='Email Address'
              className={`form-item ${isFocusEmail && 'form-item--isFocus'}`}
              rules={[
                { required: true, message: 'Require' },
                { type: 'email', message: 'Must be a valid email address' }
              ]}
            >
              <Input value={formData.email} onChange={(e) => handleChange(e,'email')} onBlur={handleFocus('email', false)} onFocus={handleFocus('email', true)} />
            </Form.Item>
            <Form.Item
              name='password'
              label='Password'
              className={`form-item ${isFocusPass && 'form-item--isFocus'}`}
              rules={[{ required: true, message: 'Require' }]}
            >
              <Input.Password value={formData.pass} onChange={(e) => handleChange(e,'pass')} onBlur={handleFocus('pass', false)} onFocus={handleFocus('pass', true)}/>
            </Form.Item>

            <Form.Item
              name='passwordConfirm'
              label='Password Confirm'
              className={`form-item ${isFocusConfirmPass && 'form-item--isFocus'}`}
              rules={[
                { required: true, message: 'Require' },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      new Error('The two passwords that you entered do not match!')
                    );
                  }
                })
              ]}
            >
              <Input.Password value={formData.confirmPass} onChange={(e) => handleChange(e,'confirmPass')} onBlur={handleFocus('confirmPass', false)} onFocus={handleFocus('confirmPass', true)}/>
            </Form.Item>
            <Form.Item>
              <NewButton loading={isLoading} text='Sign Up' type='primary' />
            </Form.Item>
          </Form>
          <div className='signup-screen__line'>
            <div className='line'></div>
            <div className='text'>or</div>
          </div>
          <div className='signup-screen__options'>
            <h5>Sign up with:</h5>
            <div className='signup-screen__options__list'>
              <div className='signup-screen__options__list-item option-google'>
                <img src='/images/google-logo.png' alt='' onClick={onClickSocial(googleProvider)}/>
              </div>
              {/* <div className='signup-screen__options__list-item option-facebook'>
                <FaFacebook />
              </div>
              <div className='signup-screen__options__list-item option-apple'>
                <FaApple />
              </div> */}
            </div>
          </div>
          <div className='signup-screen__direction'>
            <p>Already have an account? </p>
            <Link href='/login'>
              <a>Log In</a>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
};

export default SignUpScreen;
