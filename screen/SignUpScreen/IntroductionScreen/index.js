import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import NewButton from 'components/Common/NewButton';
import { Button, Form, Input, Radio } from 'antd';
import Link from 'next/dist/client/link';
import { motion } from 'framer-motion';
import RadioButtonGroup from 'components/Common/NewRadioButtonGroup';
import NewSelected from 'components/Common/NewSelected';
import NewCheckBoxGroup from 'components/Common/NewCheckBoxGroup';
import './style.scss';
import { setIntroductionActive } from 'controller/redux/action/pageActions';
import { useDispatch } from 'react-redux';

const IntroductionScreen = () => {
  const dispatch = useDispatch();
  const element = useRef(null);
  const router = useRouter();
  const idQuery = router.query.id;
  const [isValueInput, setIsValueInput] = useState('');
  const [isValueSelected, setIsValueSelected] = useState('');

  const [valueName, setValueName] = useState('');
  const [valueAge, setValueAge] = useState('');
  const [valueRetire, setValueRetire] = useState('');
  const [valueRadio, setValueRadio] = useState('');
  const [valueCheckBox, setValueCheckBox] = useState([]);

  const [infoCustomer, setInfoCustomer] = useState({});
  const [isAnimation, setIsAnimation] = useState(true);
  const listItemRadio = [
    { name: 'Very confident', value: 'a' },
    { name: 'Confident', value: 'b' },
    { name: 'Concerned', value: 'c' },
    { name: 'Very Concerned', value: 'd' }
  ];

  const listItemSelected = [
    { name: 'Very Conservative', value: 'a' },
    { name: 'Conservative', value: 'b' },
    { name: 'Moderately Conservative', value: 'c' },
    { name: 'Moderately', value: 'd' },
    { name: 'Moderately Aggressive', value: 'e' },
    { name: 'Aggressive', value: 'f' },
    { name: 'Very Aggressive', value: 'g' }
  ];

  const listItemSave = [
    { name: '401k or other employer plan', value: 'a' },
    { name: 'IRA', value: 'b' },
    { name: 'Brokerage account', value: 'c' },
    { name: 'Pension plan', value: 'd' },
    { name: 'Savings', value: 'e' },
    { name: 'Other', value: 'f' }
  ];

  const onChangeSelected = (value) => {
    setIsValueSelected(value);
  };

  const regexName = new RegExp(`^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$`);
  const onChangeName = (e) => {
    if (regexName.test(e.target.value)) {
      setValueName(e.target.value);
    }
  };

  const onChangeAge = (e) => {
    if (e.target.value > 17 && e.target.value < 101) {
      setValueAge(e.target.value);
    } else {
      setValueAge('');
    }
  };

  const onChangeRetire = (e) => {
    if (e.target.value > 34 && e.target.value < 101) {
      setValueRetire(e.target.value);
    } else {
      setValueRetire('');
    }
  };

  const onChangeRadio = (e) => {
    setValueRadio(e.target.value);
  };

  const onChangeCheckBox = (value) => {
    setValueCheckBox(value);
  };

  const progressBarStep = useMemo(() => {
    if (idQuery == 1) {
      return 0;
    } else if (idQuery == 2) {
      return 25;
    } else if (idQuery == 3) {
      return 37.5;
    } else if (idQuery == 4) {
      return 50;
    } else if (idQuery == 5) {
      return 62.5;
    } else if (idQuery == 6) {
      return 75;
    }
  }, [idQuery]);

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: { duration: 2, delay: 2 },
      transitionEnd: { display: 'none' }
    }
  };

  const hiddenContainer = {
    hidden: { opacity: 1 },
    show: {
      opacity: 1,
      transition: { delay: 5 },
      transitionEnd: { display: 'none' }
    }
  };

  const item = {
    hidden: { opacity: 0 },
    show: { opacity: 1, transition: { duration: 0.5, delay: 3 } }
  };

  // If info user have name, age, retire -> redirect to sign up user
  if (infoCustomer.valueName && infoCustomer.valueAge && infoCustomer.valueRetire) {
    dispatch(setIntroductionActive(true));
  }

  useEffect(() => {
    setInfoCustomer({ valueName, valueAge, valueRetire });
  }, [valueName, valueAge, valueRetire]);

  return (
    <div>
      <main className='introduction-screen'>
        <div className='process-bar'>
          <div className='filter' style={{ width: `${progressBarStep}%` }}></div>
        </div>
        {idQuery == 1 && (
          <div className='introduction-screen__content-name'>
            <div className='introduction-screen__wrapper'>
              <h3>Let's get to know you...</h3>
              <h4>My first name is:</h4>
              <Form name='name' initialValues={{ remember: true }}>
                <Form.Item
                  name='firstName'
                  className='form-item'
                  rules={[
                    { required: true, message: 'Require' },
                    {
                      required: true,
                      pattern: regexName,
                      message: 'Must be 35 characters or less and without spaces'
                    }
                  ]}
                >
                  <Input
                    onChange={onChangeName}
                    defaultValue={valueName}
                    placeholder='First Name'
                  />
                </Form.Item>
                <Link href='/signup/introduction/2'>
                  <NewButton
                    text='Next'
                    type='primary'
                    className='btn-next'
                    disabled={!valueName.length}
                  />
                </Link>
              </Form>
              <div className='introduction-screen__direction'>
                <p>Already have an account? </p>
                <Link href='/login'>
                  <a>Log In</a>
                </Link>
              </div>
            </div>
          </div>
        )}

        {idQuery == 2 && (
          <div>
            <motion.div
              variants={hiddenContainer}
              initial='hidden'
              animate='show'
              className='animating-container'
            >
              <div className='background-transition'></div>
              <motion.div
                variants={container}
                initial='hidden'
                animate='show'
                className='inner-text'
              >
                <motion.h1>Hi, {valueName}</motion.h1>
                <motion.p variants={item}>Just a few more questions...</motion.p>
              </motion.div>
            </motion.div>
            <div className='introduction-screen__content-age'>
              <div className='introduction-screen__wrapper'>
                <h4>How old are you ?</h4>
                <Form name='basic' initialValues={{ remember: true }}>
                  <Form.Item
                    name='age'
                    className='form-item'
                    rules={[
                      { required: true, message: 'Require' },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (getFieldValue('age') > 17 && getFieldValue('age') < 101) {
                            return Promise.resolve();
                          }
                          return Promise.reject(new Error('Value must be between 18 and 100'));
                        }
                      })
                    ]}
                  >
                    <Input onChange={onChangeAge} defaultValue={valueAge} placeholder='Age' />
                  </Form.Item>
                  <Link href='/signup/introduction/3'>
                    <NewButton
                      text='Next'
                      className='btn-next'
                      type='primary'
                      disabled={!valueAge.length}
                    />
                  </Link>
                </Form>
                <div className='introduction-screen__direction'>
                  <p>Already have an account? </p>
                  <Link href='/login'>
                    <a>Log In</a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        )}
        {idQuery == 3 && (
          <div className='introduction-screen__content-retire'>
            <div className='introduction-screen__wrapper'>
              <h4>At what age do you expect to retire?</h4>
              <Form name='basic' initialValues={{ remember: true }}>
                <Form.Item
                  name='ageRetire'
                  className='form-item'
                  rules={[
                    { required: true, message: 'Require' },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (getFieldValue('ageRetire') > 34 && getFieldValue('ageRetire') < 101) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Value must be between 35 and 100'));
                      }
                    })
                  ]}
                >
                  <Input
                    onChange={onChangeRetire}
                    defaultValue={valueRetire}
                    placeholder='Retirement'
                  />
                </Form.Item>
                <Link href='/signup/introduction/4'>
                  <NewButton
                    text='Next'
                    className='btn-next'
                    type='primary'
                    disabled={!valueRetire.length}
                  />
                </Link>
              </Form>
              <div className='introduction-screen__direction'>
                <p>Already have an account? </p>
                <Link href='/login'>
                  <a>Log In</a>
                </Link>
              </div>
            </div>
          </div>
        )}

        {idQuery == 4 && (
          <div className='introduction-screen__content-feel'>
            <div className='introduction-screen__wrapper'>
              <h4>How do you feel about your ability to retire at that age?</h4>
              <RadioButtonGroup
                listItem={listItemRadio}
                defaultValue={valueRadio}
                onChange={onChangeRadio}
              />

              <Link href='/signup/introduction/5'>
                <NewButton
                  text='Next'
                  className='btn-next'
                  type='primary'
                  disabled={!valueRadio.length}
                />
              </Link>
              <div className='introduction-screen__direction'>
                <p>Already have an account? </p>
                <Link href='/login'>
                  <a>Log In</a>
                </Link>
              </div>
            </div>
          </div>
        )}

        {idQuery == 5 && (
          <div className='introduction-screen__content__risk'>
            <div className='introduction-screen__wrapper'>
              <h4>How much risk are you comfortable with?</h4>
              <NewSelected listItem={listItemSelected} onChange={onChangeSelected} />

              {isValueSelected && (
                <div className='introduction-screen__content__risk-level'>
                  <div className='introduction-screen__content__risk-ratio'>
                    <div className='ratio'>
                      <p>Stocks: 78%</p>
                      <p>Bonds: 22%</p>
                    </div>
                    <p>Chart</p>
                  </div>
                  <div className='introduction-screen__content__risk-desc'>
                    <p>
                      A very conservative investor may often stress and worry about the ups and
                      downs of the stock market. Media headlines may tempt these investors to make
                      big changes to their investments in order to avoid the pain of a declining
                      balance.
                    </p>
                    <p>
                      Along with the added peace of mind and stability of a very conservative
                      approach, you understand that the trade-off will likely mean lower returns
                      over time.
                    </p>
                    <p className='disclosure-text'>
                      Investing involves risk. Your investments are subject to loss of principal and
                      are not guaranteed.
                    </p>
                  </div>
                </div>
              )}
              <div className='introduction-screen__quiz'>
                <Link href='/signup/risk/1'>
                  <a href=''>
                    <b>Not sure ? Take quiz</b>
                  </a>
                </Link>
              </div>
              <Link href='/signup/introduction/6'>
                <NewButton
                  text='Next'
                  className='btn-next'
                  type='primary'
                  disabled={!isValueSelected.length}
                />
              </Link>
              <div className='introduction-screen__direction'>
                <p className=''>Already have an account? </p>
                <Link href='/login'>
                  <a>Log In</a>
                </Link>
              </div>
            </div>
          </div>
        )}

        {idQuery == 6 && (
          <div className='introduction-screen__content__save'>
            <div className='introduction-screen__wrapper'>
              <h4>How are you currently saving for retirement?</h4>
              <p className='introduction-screen__title-desc'>Select all that apply</p>
              <div className=''>
                <NewCheckBoxGroup
                  listItem={listItemSave}
                  defaultValue={valueCheckBox}
                  onChange={onChangeCheckBox}
                />
              </div>

              <Link href='/signup'>
                <NewButton
                  text='Next'
                  className='btn-next'
                  type='primary'
                  disabled={!valueCheckBox.length}
                />
              </Link>
              <div className='introduction-screen__direction'>
                <p>Already have an account? </p>
                <Link href='/login'>
                  <a>Log In</a>
                </Link>
              </div>
            </div>
          </div>
        )}
      </main>
    </div>
  );
};

export default IntroductionScreen;
