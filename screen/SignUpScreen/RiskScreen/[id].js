import React, { useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import { Collapse } from 'antd';

import Link from 'next/link';
import NewButton from 'components/Common/NewButton';
import NewRadioButtonGroup from 'components/Common/NewRadioButtonGroup';
import './style.scss';

const RiskScreen = () => {
  const router = useRouter();
  const idQuery = router.query.id;
  const { Panel } = Collapse;

  const [valueStepOne, setValueStepOne] = useState('');
  const [valueStepTwo, setValueStepTwo] = useState('');
  const [valueStepThree, setValueStepThree] = useState('');
  const [valueStepFour, setValueStepFour] = useState('');
  const [valueStepFive, setValueStepFive] = useState('');

  const listItemStepOne = [
    { name: 'Limited. Stock bond what?', value: 'a' },
    { name: `Pretty good. I've got basics down?`, value: 'b' },
    { name: `Expert. If you'd like to discuss Alpha, give me a ring`, value: 'c' }
  ];
  const listItemStepTwo = [
    { name: 'Smaller pay increases but offered more job security.', value: 'a' },
    { name: `Average pay increases and offered average job security.`, value: 'b' },
    { name: `Bigger pay increases but offered less job security.`, value: 'c' }
  ];
  const listItemStepThree = [
    { name: 'Chilling in a beach chair.', value: 'a' },
    { name: `Taking in the sites on a guided tour.`, value: 'b' },
    { name: `Skydiving or other equally fun extreme sport.`, value: 'c' }
  ];
  const listItemStepFour = [
    { name: 'Eek! I’d stop contributing immediately.', value: 'a' },
    { name: `I rarely check on it, so I probably wouldn't notice.`, value: 'b' },
    { name: `I'd increase contributions! (If budget allowed.`, value: 'c' }
  ];
  const listItemStepFive = [
    { name: 'I’d pull out of the market. Money, meet mattress.', value: 'a' },
    { name: `News schmews. I don’t follow, so I’d likely do nothing.`, value: 'b' },
    { name: `Buy! Buy! Buy!`, value: 'c' }
  ];

  const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

  const onChangeStepOne = (e) => {
    setValueStepOne(e.target.value);
  };
  const onChangeStepTwo = (e) => {
    setValueStepTwo(e.target.value);
  };
  const onChangeStepThree = (e) => {
    setValueStepThree(e.target.value);
  };
  const onChangeStepFour = (e) => {
    setValueStepFour(e.target.value);
  };
  const onChangeStepFive = (e) => {
    setValueStepFive(e.target.value);
  };

  const progressBarStep = useMemo(() => {
    if (idQuery == 1) {
      return 0;
    } else if (idQuery == 2) {
      return 33.3333;
    } else if (idQuery == 3) {
      return 50;
    } else if (idQuery == 4) {
      return 66.6667;
    } else if (idQuery == 5) {
      return 83.3333;
    }
  }, [idQuery]);

  return (
    <main className='risk-screen'>
      <div className='process-bar'>
        <div className='filter' style={{ width: `${progressBarStep}%` }}></div>
      </div>
      {idQuery == 1 && (
        <div className='risk-screen__content__step-1'>
          <div className='risk-screen__wrapper'>
            <h3>I would describe my knowledge of stocks and bonds as:</h3>
            <NewRadioButtonGroup
              listItem={listItemStepOne}
              defaultValue={valueStepOne}
              onChange={onChangeStepOne}
            />

            <div className='risk-screen__more'>
              <Collapse expandIconPosition={'right'}>
                <Panel header='Tell Me More ...' key='1'>
                  <p>{text}</p>
                </Panel>
              </Collapse>
            </div>
            <Link href='/signup/risk/2'>
              <NewButton
                text='Next'
                type='primary'
                className='btn-next'
                disabled={!valueStepOne.length}
              />
            </Link>
            <div className='risk-screen__direction'>
              <p>Already have an account? </p>
              <Link href='/login'>
                <a>Log In</a>
              </Link>
            </div>
          </div>
        </div>
      )}
      {idQuery == 2 && (
        <div className='risk-screen__content__step-2'>
          <div className='risk-screen__wrapper'>
            <h3>If I had the choice I would choose a job with a company that made:</h3>
            <NewRadioButtonGroup
              listItem={listItemStepTwo}
              defaultValue={valueStepTwo}
              onChange={onChangeStepTwo}
            />

            <div className='risk-screen__more'>
              <Collapse expandIconPosition={'right'}>
                <Panel header='Tell Me More ...' key='1'>
                  <p>{text}</p>
                </Panel>
              </Collapse>
            </div>
            <Link href='/signup/risk/3'>
              <NewButton
                text='Next'
                type='primary'
                className='btn-next'
                disabled={!valueStepTwo.length}
              />
            </Link>
            <div className='risk-screen__direction'>
              <p>Already have an account? </p>
              <Link href='/login'>
                <a>Log In</a>
              </Link>
            </div>
          </div>
        </div>
      )}
      {idQuery == 3 && (
        <div className='risk-screen__content__step-3'>
          <div className='risk-screen__wrapper'>
            <h3>My ideal vacation would include:</h3>
            <NewRadioButtonGroup
              listItem={listItemStepThree}
              defaultValue={valueStepThree}
              onChange={onChangeStepThree}
            />

            <div className='risk-screen__more'>
              <Collapse expandIconPosition={'right'}>
                <Panel header='Tell Me More ...' key='1'>
                  <p>{text}</p>
                </Panel>
              </Collapse>
            </div>
            <Link href='/signup/risk/4'>
              <NewButton
                text='Next'
                type='primary'
                className='btn-next'
                disabled={!valueStepThree.length}
              />
            </Link>
            <div className='risk-screen__direction'>
              <p>Already have an account? </p>
              <Link href='/login'>
                <a>Log In</a>
              </Link>
            </div>
          </div>
        </div>
      )}
      {idQuery == 4 && (
        <div className='risk-screen__content__step-4'>
          <div className='risk-screen__wrapper'>
            <h3>
              Imagine that your retirement balance has declined 20% in the past 3 months, what would
              you do to your contributions?
            </h3>
            <NewRadioButtonGroup
              listItem={listItemStepFour}
              defaultValue={valueStepFour}
              onChange={onChangeStepFour}
            />

            <div className='risk-screen__more'>
              <Collapse expandIconPosition={'right'}>
                <Panel header='Tell Me More ...' key='1'>
                  <p>{text}</p>
                </Panel>
              </Collapse>
            </div>
            <Link href='/signup/risk/5'>
              <NewButton
                text='Next'
                type='primary'
                className='btn-next'
                disabled={!valueStepFour.length}
              />
            </Link>
            <div className='risk-screen__direction'>
              <p>Already have an account? </p>
              <Link href='/login'>
                <a>Log In</a>
              </Link>
            </div>
          </div>
        </div>
      )}
      {idQuery == 5 && (
        <div className='risk-screen__content__step-5'>
          <div className='risk-screen__wrapper'>
            <h3>
              You turn on the news and hear that the market has dropped 200 points. Which answer is
              closest to your response:
            </h3>
            <NewRadioButtonGroup
              listItem={listItemStepFive}
              defaultValue={valueStepFive}
              onChange={onChangeStepFive}
            />

            <div className='risk-screen__more'>
              <Collapse expandIconPosition={'right'}>
                <Panel header='Tell Me More ...' key='1'>
                  <p>{text}</p>
                </Panel>
              </Collapse>
            </div>
            <Link href='/signup/risk-tolerance'>
              <NewButton
                text='Next'
                type='primary'
                className='btn-next'
                disabled={!valueStepFive.length}
              />
            </Link>
            <div className='risk-screen__direction'>
              <p>Already have an account? </p>
              <Link href='/login'>
                <a>Log In</a>
              </Link>
            </div>
          </div>
        </div>
      )}
    </main>
  );
};

export default RiskScreen;
