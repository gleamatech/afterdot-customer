import NewButton from 'components/Common/NewButton';
import NewSelected from 'components/Common/NewSelected';
import React, { useState } from 'react';
import Link from 'next/link';
import './style.scss';

const RiskToleranceScreen = () => {
  const [valueSelected, setValueSelected] = useState('');
  const onChangeRadio = (e) => {
    setValueRadio(e.target.value);
  };

  const listItemSelected = [
    { name: 'Very Conservative', value: 'a' },
    { name: 'Conservative', value: 'b' },
    { name: 'Moderately Conservative', value: 'c' },
    { name: 'Moderately', value: 'd' },
    { name: 'Moderately Aggressive', value: 'e' },
    { name: 'Aggressive', value: 'f' },
    { name: 'Very Aggressive', value: 'g' }
  ];
  const onChangeSelected = (value) => {
    setIsValueSelected(value);
  };
  return (
    <main className='risk-tolerance-screen'>
      <div className='container'>
        <div className='risk-tolerance-screen__comfortable'>
          <div className='risk-tolerance-screen__wrapper'>
            <h4>Based on what you told us...</h4>
            <p className='risk-tolerance-screen__title-desc'>
              We think you will be most comfortable with <span>Conservative</span> portfolio
              allocation.
            </p>
            <div className='risk-tolerance-screen__risk'>
              <p>How much risk are you comfortable with?</p>
              <NewSelected listItem={listItemSelected} onChange={onChangeSelected} />

              <div className='risk-tolerance-screen__comfortable-level'>
                <div className='risk-tolerance-screen__comfortable-ratio'>
                  <div className='ratio'>
                    <p>Stocks: 78%</p>
                    <p>Bonds: 22%</p>
                  </div>
                  <p>Chart</p>
                </div>
                <div className='risk-tolerance-screen__comfortable-desc'>
                  <p>
                    A very conservative investor may often stress and worry about the ups and downs
                    of the stock market. Media headlines may tempt these investors to make big
                    changes to their investments in order to avoid the pain of a declining balance.
                  </p>
                  <p>
                    Along with the added peace of mind and stability of a very conservative
                    approach, you understand that the trade-off will likely mean lower returns over
                    time.
                  </p>
                </div>
              </div>
            </div>
            <div className='risk-tolerance-screen__button'>
              <Link href='/signup/introduction/6'>
                <NewButton
                  text='Next'
                  className='btn-next'
                  type='primary'
                  disabled={!valueSelected.length}
                />
              </Link>
            </div>

            <div className='risk-tolerance-screen__quiz'>
              <Link href='/signup/risk/1'>
                <a href=''>
                  <b>Retake the Quiz</b>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default RiskToleranceScreen;
