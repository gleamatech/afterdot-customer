import { Row, Col } from 'antd';
import NewButton from 'components/Common/NewButton';
import Lottie from 'react-lottie';
import React from 'react';
import HandHoldPhone from 'public/lotties/hand-hold-a-phone.json';
import PiggyScroller from 'public/lotties/piggy-scroller.json';
import MeetTheTeam from 'public/lotties/meet-the-team-hand-waves.json';
import './style.scss';

const FinancialAdvsiorScreen = () => {
  return (
    <main className='financialadvisorscreen'>
      <div className='financialadvisorscreen__first-content'>
        <div className='container'>
          <div className='financialadvisorscreen__first-content-wrapper'>
            <Row className='financialadvisorscreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='financialadvisorscreen__content__col financialadvisorscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>FIDUCIARY FINANCIAL ADVISOR NEAR ME</h1>
                <h2>Who is blooom?</h2>
                <p>Your online financial advisor.</p>
                <p>
                  Blooom is a dedicated bunch of financial, technical, and business minds working
                  toward a common goal of making expert financial advice available to all. Our
                  company was inspired by the frustrations and shortfalls of the financial industry
                  and motivated by the potential of ever-changing technology.
                </p>
                <p>
                  We are real financial advisors with decades of experience in retirement planning
                  and 401k management. Unfortunately, the longer you go without getting advice you
                  need, the less time you have to get it right. And getting the right advice can
                  make all the difference in retirement.
                </p>
              </Col>
              <Col
                xs={12}
                className='financialadvisorscreen__content__col financialadvisorscreen__content__col-image'
              >
                <Lottie
                  size={350}
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: MeetTheTeam
                  }}
                />
                <div className='sky1'>
                  <img src='/images/Pink-Cloud-Second.svg' />
                </div>
                <div className='sky2'>
                  <img src='/images/Pink-Cloud.svg' />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <div className='financialadvisorscreen__third-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='financialadvisorscreen__third-content-top'>
              <h2>We are…</h2>
            </div>
            <div className='financialadvisorscreen__third-content-wrapper'>
              <Row className='financialadvisorscreen__content' gutter={[50, 0]}>
                <Col
                  xs={8}
                  className='financialadvisorscreen__content__col financialadvisorscreen__content__col-text'
                >
                  <h4>mission-driven.</h4>
                  <p>
                    Dedicated to bringing expert advice to the masses—whether it be via optimizing
                    your 401k or education around tough topics.
                  </p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>fiduciaries.</h4>
                  <p>
                    Unlike broker/dealers who make a commission off of a recommendation, we are
                    legally (and morally) obligated to work in our clients’ best interest.
                  </p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>human experts.</h4>
                  <p>
                    While our technology is a big part of our team, our portfolio is built and
                    powered by real human financial advisors, and a team of tech and account
                    experts.
                  </p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
      <div className='financialadvisorscreen__four-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='financialadvisorscreen__four-content-top'>
              <h2>Meet some of our humans</h2>
            </div>
            <div className='financialadvisorscreen__four-content-wrapper'>
              <Row className='financialadvisorscreen__content' gutter={[50, 0]}>
                <Col
                  xs={8}
                  className='financialadvisorscreen__content__col financialadvisorscreen__content__col-text'
                >
                  <h4>Chris C.</h4>
                  <p>Wall Street Whistleblower • Penny Preacher • Family Maniac</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Brandi R.</h4>
                  <p>Nerf Gun Sniper • Weekend Kayaker • History Buff</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Andrew T.</h4>
                  <p>LaCroix Connoisseur • Girl Dad of 2 • KC Sports Super Fan</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Chad B.</h4>
                  <p>Beer Enthusiast • Certified Rescue Diver • Globe Trotter</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Chrissy E.</h4>
                  <p>Addicted to Travel • Bleeds Queso • Family Fanatic</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Laura W.</h4>
                  <p>Nature Lover • Caring Counselor • World Traveler</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Shannon D.</h4>
                  <p>Rule Follower • Shameless Carpool Singer • Cool Suburban Mom</p>
                </Col>
                <Col xs={8} className='financialadvisorscreen__content__col'>
                  <h4>Zach D.</h4>
                  <p>Long Distance Canoer • Girl Dad of 3 • Avid Golfer</p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='financialadvisorscreen__six-content'>
        <div className='container'>
          <div className='financialadvisorscreen__six-content-wrapper' data-aos='fade-down'>
            <h2>Start with a complementary investment strategy.</h2>
            <NewButton text='Build my dashboard' type='primary' />
          </div>
        </div>
      </div>
    </main>
  );
};

export default FinancialAdvsiorScreen;
