import { Row, Col } from 'antd';
import NewButton from 'components/Common/NewButton';
import Lottie from 'react-lottie';
import React from 'react';
import HandHoldPhone from 'public/lotties/hand-hold-a-phone.json';
import PiggyScroller from 'public/lotties/piggy-scroller.json';
import './style.scss';

const RoboInvestingScreen = () => {
  return (
    <main className='investingscreen'>
      <div className='investingscreen__first-content'>
        <div className='container'>
          <div className='investingscreen__first-content-wrapper'>
            <Row className='investingscreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='investingscreen__content__col investingscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>ROBO INVESTING </h1>
                <h2>Taking robo advising to the next level.</h2>

                <p>
                  Some say we simply offer robo advising for your 401k. Others say we lower fees in
                  their IRA. But the real reason we exist is to make expert financial advice
                  available to all. Our clients come for investment optimization and stay for the
                  peace of mind. They trust blooom’s real, unbiased advisors have their back, today,
                  and all the way to retirement.
                </p>
              </Col>
              <Col
                xs={12}
                className='investingscreen__content__col investingscreen__content__col-image'
              >
                <Lottie
                  size={350}
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: HandHoldPhone
                  }}
                />
                <div className='sky1'>
                  <img src='/images/Pink-Cloud-Second.svg' />
                </div>
                <div className='sky2'>
                  <img src='/images/Pink-Cloud.svg' />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <div className='investingscreen__second-benefit'>
        <div className='container'>
          <div className='investingscreen__second-benefit-wrapper'>
            <Row className='investingscreen__content' gutter={[50, 0]}>
              <Col
                xs={16}
                className='investingscreen__content__col investingscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>What can blooom do for you ?</h1>
                <h3>Create a personal investment strategy</h3>
                <p>
                  The first step in your retirement journey is understanding where you’re headed.
                  Based on your answers to a few simple questions, blooom’s experts can create a
                  personal investment strategy… and help you stick to it.
                </p>
                <h3>Do the research</h3>
                <p>
                  Remember the confusion that struck the first time you logged into your 401k and
                  hit a wall of fund jibberish? We can translate that. Better yet, we’ll match our
                  fund research to your personal strategy and give you the optimal plan for your
                  goals.
                </p>
                <h3>Pick & place the trades</h3>
                <p>
                  Unlike Wall Street, we don’t make money off the funds in your account. Our robo
                  advising technology allows us to make recommendations that benefit you… and only
                  you! Let blooom place the trades on your behalf, or take the lineup and implement
                  yourself if you’re more comfortable.
                </p>
                <h3>Perform ongoing optimization</h3>
                <p>
                  You have your oil checked, you have your teeth checked, but chances are you don’t
                  regularly perform maintenance on your most important retirement asset. We’re here
                  to regularly monitor your accounts and make tweaks to optimize as necessary. We
                  provide 3 main services when optimizing your retirement accounts:
                </p>
                <ul>
                  <li>
                    <strong>Risk management</strong> – Is your stock to bond ratio appropriate for
                    your age to retirement?
                  </li>
                  <li>
                    <strong>Diversified portfolio</strong> – Are you invested in a range of asset
                    classes?
                  </li>
                  <li>
                    <strong>Fee minimization</strong> – Are you paying too much in hidden investment
                    fees?
                  </li>
                </ul>

                <h3>Watch the market</h3>
                <p>
                  The road to retirement can be a bumpy one. Blooom’s team & robo advising tools are
                  here to be your partner through the ups and downs. Remember, we can’t control the
                  market, but we can control how—or if—we react. Our advisors are available to help
                  you stay the course on your personal strategy.
                </p>
                <h3>Add an extra layer of security</h3>
                <p>
                  Blooom adds an extra layer of protection to your retirement account. Withdrawal
                  alerts notify you of any unusual activity in your 401k or IRA, keeping your
                  hard-earned nest egg safe and secure.
                </p>
                <h3>Be your unbiased financial expert</h3>
                <p>
                  Whether you’re wondering whether to do a rollover into your new employer’s plan,
                  how to start an HSA or even if buying a new puppy is in the budget, we’re here to
                  help you wade through the financial decisions. We believe making smart financial
                  choices should extend beyond your retirement account!
                </p>
              </Col>
              <Col
                xs={8}
                className='investingscreen__content__col investingscreen__content__col-image'
                data-aos='fade-down'
              >
                <Lottie
                  className='sticky-image'
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: PiggyScroller
                  }}
                />
              </Col>
            </Row>
            <div className='sky1'>
              <img src='/images/white-cloud.png' />
            </div>
            <div className='sky2'>
              <img src='/images/white-cloud.png' />
            </div>
          </div>
        </div>
      </div>

      <div className='investingscreen__third-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='investingscreen__third-content-top'>
              <h2>The blooom strategy</h2>
              <p>
                We can’t control the market, but we can control how we invest in it. Blooom’s
                people-driven robo advising strategy focuses on getting you set up properly,
                proactively keeping you on track, and making ourselves available for anything that
                might pop up along the way.
              </p>
            </div>
            <div className='investingscreen__third-content-wrapper'>
              <Row className='investingscreen__content' gutter={[50, 0]}>
                <Col
                  xs={8}
                  className='investingscreen__content__col investingscreen__content__col-text'
                >
                  <img src='/images/On-Target.svg' alt='' />
                  <h4>On Target</h4>
                  <p>
                    Beyond the legal binds of being an SEC Registered Investment Advisor, and
                    fiduciary, we treat our clients like our best friends and family.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <img src='/images/On-Track.svg' alt='' />
                  <h4>On Track</h4>
                  <p>
                    Time IN the market is more important than time-ING the market. Look to us when
                    any of your financial questions pop up.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <img src='/images/On-your-Side.svg' alt='' />
                  <h4>On your side</h4>
                  <p>
                    Our team is dedicated to our mission of bringing expert, unbiased financial help
                    to the masses.
                  </p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='investingscreen__four-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='investingscreen__four-content-top'>
              <h2>Account types</h2>
            </div>
            <div className='investingscreen__four-content-wrapper'>
              <Row className='investingscreen__content' gutter={[50, 0]}>
                <Col
                  xs={8}
                  className='investingscreen__content__col investingscreen__content__col-text'
                >
                  <h4>IRA</h4>
                  <p>
                    That’s right, we don’t just do 401k’s! If you have an existing IRA at Fidelity,
                    blooom can manage it! If you have an IRA at Vanguard, Charles Schwab and TD
                    Ameritrade, we can offer you an investment recommendation. Whether it’s a
                    traditional IRA, Roth IRA, SEP IRA, nondeductible IRA, Spousal IRA, Simple IRA
                    or Self-directed… We can help optimize your portfolio for your goals.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <h4>401k</h4>
                  <p>
                    If you are saving through an employer-sponsored plan, like a 401k, chances are
                    blooom can manage it. While we are limited to the funds chosen by your employer,
                    we are here to do the research, provide truly unbiased advice and build a
                    portfolio that best suits your goals. No need to move your money. We can take
                    care of it right where it is!
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <h4>401a</h4>
                  <p>
                    Many government and non-profit organizations offer 401a plans. Once we
                    understand your tolerance for risk and retirement goal, we can research, analyze
                    and recommend the ideal funds for your situation. Choose to have blooom manage
                    your 401a and we’ll even place the trades and monitor for changes on your behalf
                    so you’ll have peace of mind that you’re properly invested.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <h4>403b</h4>
                  <p>
                    If you work at a public school, tax-exempt organization or ministry, your 403b
                    plan can likely be managed by blooom. Start with the complimentary checkup to
                    see if you’re on track, then we can help you decide which funds are best for
                    your situation. And we won’t ask you to move your money! We can manage it right
                    where it is.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <h4>457</h4>
                  <p>
                    Your 457 plan operates similarly to a 401k or other employee sponsored account,
                    and is therefore likely manageable by blooom. We understand the key differences
                    and intricacies of working with 457s. Whether you’re an independent contractor
                    or governmental employee, we work with your account to be optimized for your
                    goals.
                  </p>
                </Col>
                <Col xs={8} className='investingscreen__content__col'>
                  <h4>TSP</h4>
                  <p>
                    Your Thrift Savings Plan (TSP) was designed to give federal employees and
                    members of the uniformed services the same retirement-savings benefits as
                    private sector workers. We have seen thousands of TSPs and would love to help
                    you optimize yours.
                  </p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='investingscreen__five-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='investingscreen__five-content-wrapper'>
              <Row className='investingscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>Are you an Aggressive Alex?</h2>
                  <p>Living on the edge of retirement.</p>
                  <p>
                    <strong>Scenario:</strong> An Alex will be retiring in a few short years
                    (hallelujah!), but has nearly their entire 401k invested in stocks (yikes!).
                  </p>
                  <p>
                    <strong>How blooom helps:</strong> A market drop could destroy Alex’s plan and
                    ability to retire. We adjust Alex’s account to include funds that can withstand
                    a myriad of market scenarios to provide more stability and better prepare them
                    for retirement.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-image'
                >
                  <img src='/images/Agressive-Andy.svg' />
                </Col>
              </Row>
            </div>
            <div className='investingscreen__five-content-wrapper'>
              <Row className='investingscreen__content ' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>Or maybe you’re a Busy Blake.</h2>
                  <p>
                    Believes weekends are meant for things other than optimizing their investments.
                  </p>
                  <p>
                    <strong>Scenario:</strong> A Blake has decent grasp on their retirement
                    portfolio. They could do it themselves and be alright, but they don’t have the
                    time, and time is precious!
                  </p>
                  <p>
                    <strong>How blooom helps:</strong> Blakes can hand the work over to blooom and
                    rest assured knowing they’re getting a professionally managed account for a
                    fraction of the cost of a traditional advisor. This frees them up to spend their
                    time doing what they enjoy.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-image'
                >
                  <img src='/images/Busy-Bob.svg' />
                </Col>
              </Row>
            </div>

            <div className='investingscreen__five-content-wrapper'>
              <Row className='investingscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>Feeling like a Target Taylor?</h2>
                  <p>
                    Believes weekends are meant for things other than optimizing their investments.
                  </p>
                  <p>
                    <strong>Scenario:</strong> A Taylor is invested in a target date fund.
                  </p>
                  <p>
                    <strong>How blooom helps:</strong> Taylors have the right idea on
                    diversification, but is likely paying too much for it. Some blooom clients who
                    were in a target-date fund pre-blooom reduced their fees with their new blooom
                    allocation.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-image'
                >
                  <img src='/images/Target-Date-Nate.svg' />
                </Col>
              </Row>
            </div>
            <div className='investingscreen__five-content-wrapper'>
              <Row className='investingscreen__content ' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>You may be a Conservative Casey.</h2>
                  <p>Missing out on all the fun… and potential returns.</p>
                  <p>
                    <strong>Scenario:</strong> A Casey is decades from retirement but has a large
                    portion of their account invested in bonds and other conservative options.
                  </p>
                  <p>
                    <strong>How blooom helps</strong>: We put them in a proper (more growth-focused)
                    allocation hopefully increasing their long-term return.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-image'
                >
                  <img src='/images/Conservative-Connie.svg' />
                </Col>
              </Row>
            </div>
            <div className='investingscreen__five-content-wrapper'>
              <Row className='investingscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>Don’t be a Random Riley.</h2>
                  <p>Picks funds like they’re picking teams on the playground.</p>
                  <p>
                    <strong>Scenario:</strong> A Riley has no idea what to do when they’re handed a
                    401k, so they pick the 5 funds that have performed the best over the past year.
                  </p>
                  <p>
                    <strong>How blooom helps:</strong> Little does Riley know, these funds are
                    probably all very similar, leaving them way too concentrated. We’re going to
                    make sure they’re properly diversified and possibly reduce their fees in the
                    process.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='investingscreen__content__col investingscreen__content__col-image'
                >
                  <img src='/images/Wackie-Jackie.svg' />
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='investingscreen__six-content'>
        <div className='container'>
          <div className='investingscreen__six-content-wrapper' data-aos='fade-down'>
            <h2>Start with a complementary investment strategy.</h2>
            <NewButton text='Build my dashboard' type='primary' />
          </div>
        </div>
      </div>
    </main>
  );
};

export default RoboInvestingScreen;
