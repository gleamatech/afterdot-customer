import { Row, Col } from 'antd';
import NewButton from 'components/Common/NewButton';
import Lottie from 'react-lottie';
import React, { useState } from 'react';
import PiggyFlying from 'public/lotties/pig-flying.json';
import LaptopPlinko from 'public/lotties/laptop-plinko.json';
import PigPink from 'public/lotties/pink-pig-flying.json';
import PigYellow from 'public/lotties/yellow-pig-flying.json';
import NewCardContent from 'components/Common/NewCardContent';
import './style.scss';

const BestAdvisorScreen = () => {
  const [keyActive, setKeyActive] = useState('');

  const handleClickItem = (number) => () => {
    if (number === keyActive) {
      return setKeyActive('');
    }
    return setKeyActive(number);
  };

  const renderList = () => {
    return [1, 2, 3].map((item) => {
      return (
        <NewCardContent
          isOpen={keyActive === item}
          id={item}
          title='When Wall Street'
          className='advisorscreen__list-card-content__item'
          onClick={handleClickItem}
          key={item}
        >
          <p>
            The financial industry is broken. For years, people could rely on pensions and social
            security for retirement, both of which are scarce options today. Many American’s best
            chance at retiring comfortably lies in their 401k, which is (purposely?) confusing,
            forcing us all to fend for ourselves when it comes to understanding and investing in our
            future. For decades, professional financial advice has been reserved only for clients
            with large account balances. Wall Street brokers have preyed off those willing to
            invest, but confused by the plethora of options, leading to distrust and frustration
            with the financial industry as a whole. There’s got to be a better way.
          </p>
          <p>
            <strong>
              Blooom believes that everyone deserves access to quality, unbiased financial advice.
            </strong>{' '}
            That’s why our founders started blooom. Fed up with Wall Street’s inability to help the
            average investor, Randy AufDerHeide, Kevin Conard and Chris Costello set out to create a
            way for everyone to get basic help with their retirement assets, no matter the size of
            their account, or where it’s held. They created a company, and set of tools, that knocks
            down the barriers to entry of a traditional financial planner.
          </p>
        </NewCardContent>
      );
    });
  };
  return (
    <main className='advisorscreen'>
      <div className='advisorscreen__first-content'>
        <div className='container'>
          <div className='advisorscreen__first-content-wrapper'>
            <Row className='advisorscreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='advisorscreen__content__col advisorscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>RETIREMENT ROBO ADVISOR</h1>
                <h2>Let’s blooom together</h2>

                <p>
                  An online financial advisor should be much more than a maintenance tool. It should
                  help you continue to grow your funds and reach newer heights than you could if you
                  were to try to “DIY” your own retirement accounts.
                </p>
                <p>
                  Blooom’s philosophy is simple: we’re here for you — not your company, broker, or
                  institution. We think everyone deserves access to professional independent
                  financial advice. For those seeking professional guidance in your retirement
                  accounts, blooom’s team of expert advisors &amp; developers have worked hard to
                  create the a reliable robo advisor experience not just for the biggest accounts,
                  but for the smallest ones, too.
                </p>
              </Col>
              <Col
                xs={12}
                className='advisorscreen__content__col advisorscreen__content__col-image'
              >
                <div className='big-piggy' data-aos='fade-right'>
                  <Lottie
                    options={{
                      loop: true,
                      autoplay: true,
                      animationData: PiggyFlying
                    }}
                  />
                </div>

                <div className='small-piggy' data-aos='fade-right'>
                  <Lottie
                    width={150}
                    options={{
                      loop: true,
                      autoplay: true,
                      animationData: PigYellow
                    }}
                  />
                  <Lottie
                    width={150}
                    options={{
                      loop: true,
                      autoplay: true,
                      animationData: PigPink
                    }}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <div className='advisorscreen__second-benefit'>
        <div className='container'>
          <div className='advisorscreen__second-benefit-wrapper'>
            <Row className='advisorscreen__content' gutter={[50, 0]}>
              <Col
                xs={16}
                className='advisorscreen__content__col advisorscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>Here’s the problem…</h1>
                <h3>Investing is confusing.</h3>
                <p>
                  <strong>You shouldn’t need a PhD in finance to understand your 401k.</strong>
                </p>
                <p>
                  If you are like most of our clients, you may not be totally confident that you
                  have selected the right investments in your 401k or IRA. Who could blame you? You
                  have enough going on in your life. Most of our clients also freely admit they are
                  not confident in their investment-picking skills. You have to really enjoy
                  finances to get into this stuff. But getting your portfolio right can have
                  significant impacts on long-term growth.
                </p>
                <h3>Planning is hard.</h3>
                <p>
                  <strong>Traditional financial help is often expensive or inaccessible.</strong>
                </p>
                <p>
                  Wall Street has made a killing on inexperienced investors. We all know we need to
                  manage our retirement, but unless our account balances are high enough to draw the
                  attention of a professional, we are left to our own devices. While there’s value
                  to having an expert on your side, good advice from a true fiduciary is hard to
                  come by.
                </p>
                <h3>Blooom is simple.</h3>
                <p>
                  <strong>From investment to retirement, we’re here for you, and only you.</strong>
                </p>
                <p>
                  Regardless of account size or institution, we provide unbiased advice and
                  management for your most important retirement asset. We will make sure you are on
                  target, on track and on our side from now until retirement.
                </p>
              </Col>
              <Col xs={8} className='advisorscreen__content__col advisorscreen__content__col-image'>
                <Lottie
                  className='sticky-image'
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: LaptopPlinko
                  }}
                />
              </Col>
            </Row>
            <div className='cloud1'>
              <img src='/images/white-cloud.png' />
            </div>
            <div className='cloud2'>
              <img src='/images/white-cloud.png' />
            </div>
          </div>
        </div>
      </div>

      <div className='advisorscreen__third-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='advisorscreen__third-content-wrapper'>
              <Row className='advisorscreen__content' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='advisorscreen__content__col advisorscreen__content__col-text'
                >
                  <h1>THE NUMBERS DON'T LIE</h1>
                  <h2>Currently managing over</h2>
                  <img src='/images/5billion.png' alt='' />
                  <h2>in assets… and growing.</h2>
                </Col>
                <Col
                  xs={12}
                  className='advisorscreen__content__col advisorscreen__content__col-image'
                >
                  <img src='/images/iphoneswipe_new.png' />
                  <div className='sky1'>
                    <img src='/images/Pink-Cloud-Second.svg' />
                  </div>
                  <div className='sky2'>
                    <img src='/images/Pink-Cloud.svg' />
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='advisorscreen__list-card-content'>
        <div className='container'>
          <Row gutter={[50, 0]}>
            <Col className='advisorscreen__list-card-content__wrapper'>
              <div data-aos='fade-down'>
                <p className='advisorscreen__list-card-content__title'>
                  Let’s go through, in more depth, the “three o’s of <span>blooom</span>.”
                </p>
                <div className='advisorscreen__list-card-content__list-item'>{renderList()}</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>

      <div className='advisorscreen__four-content'>
        <div className='container'>
          <div className='advisorscreen__four-content__wrapper' data-aos='fade-down'>
            <h1>Learn how to help your retirement blooom</h1>
            <h2>Get my free investment strategy</h2>
            <NewButton text='Sign up' type='primary' />
          </div>
        </div>
      </div>
    </main>
  );
};

export default BestAdvisorScreen;
