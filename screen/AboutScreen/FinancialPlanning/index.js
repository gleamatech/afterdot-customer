import React, { useState } from 'react';
import { Row, Col } from 'antd';
import NewButton from 'components/Common/NewButton';
import './style.scss';
import NewCardContent from 'components/Common/NewCardContent';

const FinancialPlanningScreen = () => {
  const [keyActive, setKeyActive] = useState('');

  const handleClickItem = (number) => () => {
    if (number === keyActive) {
      return setKeyActive('');
    }
    return setKeyActive(number);
  };

  const renderList = () => {
    return [1, 2].map((item) => {
      return (
        <NewCardContent
          isOpen={keyActive === item}
          id={item}
          title='When Wall Street'
          className='financialscreen__list-card-content__item'
          onClick={handleClickItem}
          key={item}
        >
          <p>
            The financial industry is broken. For years, people could rely on pensions and social
            security for retirement, both of which are scarce options today. Many American’s best
            chance at retiring comfortably lies in their 401k, which is (purposely?) confusing,
            forcing us all to fend for ourselves when it comes to understanding and investing in our
            future. For decades, professional financial advice has been reserved only for clients
            with large account balances. Wall Street brokers have preyed off those willing to
            invest, but confused by the plethora of options, leading to distrust and frustration
            with the financial industry as a whole. There’s got to be a better way.
          </p>
          <p>
            <strong>
              Blooom believes that everyone deserves access to quality, unbiased financial advice.
            </strong>{' '}
            That’s why our founders started blooom. Fed up with Wall Street’s inability to help the
            average investor, Randy AufDerHeide, Kevin Conard and Chris Costello set out to create a
            way for everyone to get basic help with their retirement assets, no matter the size of
            their account, or where it’s held. They created a company, and set of tools, that knocks
            down the barriers to entry of a traditional financial planner.
          </p>
        </NewCardContent>
      );
    });
  };

  return (
    <main className='financialscreen'>
      <div className='financialscreen__first-content'>
        <div className='container'>
          <div className='financialscreen__content-wrapper'>
            <Row className='financialscreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='financialscreen__content__col financialscreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>FINANCIAL PLANNING </h1>
                <h2>
                  A bit about <span>blooom</span>
                </h2>
                <h1>
                  <strong>We’re so much more than a financial planning service.</strong>
                </h1>
                <p>
                  Blooom is an SEC Registered Investment Advisor dedicated to changing the way
                  people save for retirement. We believe everyone deserves the same expert financial
                  advice previously afforded to only the wealthy. We know saving for retirement is
                  confusing. That’s why blooom starts simply. Our unbiased experts help put your
                  portfolio on the right track—and keep it that way.
                </p>
              </Col>
              <Col
                xs={12}
                className='financialscreen__content__col financialscreen__content__col-image'
              >
                <img src='/images/Tablet-Cloud.svg' className='main-image' />

                <div className='sub-image'>
                  <img src='/images/mug.svg' />
                </div>
                <div className='sky1'>
                  <img src='/images/Pink-Cloud-Second.svg' />
                </div>
                <div className='sky2'>
                  <img src='/images/Pink-Cloud.svg' />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <div className='financialscreen__second-content'>
        <div className='container'>
          <div className='financialscreen__second-content-wrapper'>
            <div data-aos='fade-down'>
              <h3>Retirement accounts are complicated. Blooom is not.</h3>
              <p>
                Blooom is founded on the belief that professional retirement help should be smart,
                simple, and available to all Americans.
              </p>
              <iframe
                loading='lazy'
                title='Blooom - A Whole New Way to 401k Video'
                src='https://fast.wistia.net/embed/iframe/v4j8xh7ptn?dnt=1&amp;videoFoam=true'
                allow='autoplay; fullscreen'
                allowtransparency='true'
                frameborder='0'
                scrolling='no'
                allowfullscreen='allowfullscreen'
                msallowfullscreen='msallowfullscreen'
                mozallowfullscreen='mozallowfullscreen'
                webkitallowfullscreen='webkitallowfullscreen'
                oallowfullscreen='oallowfullscreen'
              ></iframe>
            </div>
          </div>
        </div>
      </div>

      <div className='financialscreen__third-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <h2>We promise…</h2>
            <div className='financialscreen__third-content-wrapper'>
              <Row className='financialscreen__content' gutter={[50, 0]}>
                <Col
                  xs={8}
                  className='financialscreen__content__col financialscreen__content__col-text'
                >
                  <h4>to always act in your best interest.</h4>
                  <p>
                    Beyond the legal binds of being an SEC Registered Investment Advisor, and
                    fiduciary, we treat our clients like our best friends and family.
                  </p>
                </Col>
                <Col xs={8} className='financialscreen__content__col'>
                  <h4>to be here for you during ups and downs.</h4>
                  <p>
                    Time IN the market is more important than time-ING the market. Look to us when
                    any of your financial questions pop up.
                  </p>
                </Col>
                <Col xs={8} className='financialscreen__content__col'>
                  <h4>to bring financial management to all!</h4>
                  <p>
                    Our team is dedicated to our mission of bringing expert, unbiased financial help
                    to the masses.
                  </p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='financialscreen__four-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>A financial app backed by real humans.</h2>
                  <p>
                    The blooom team is made up of financial advisors, tech experts, and all-around
                    business-savvy people. We live for financial literacy—taking an education-first
                    approach to any challenge that crosses our client’s path. Our team is out to
                    prove that better is possible in this wild world of finance.
                  </p>
                  <NewButton text='Meet Our Team' type='primary' />
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Meet-the-Team-Pig-Pyramid.svg' />
                </Col>
              </Row>
            </div>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content ' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h2>Why blooom? Let’s get down to it.</h2>
                  <p>
                    If you’re still not sure whether blooom’s financial retirement planning app can
                    affect your account, just ask yourself these 4 simple questions. You may be
                    surprised to find that your 401k or IRA is in need of some TLC!
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Tablet-Cloud.svg' />
                </Col>
              </Row>
            </div>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h4>
                    1. Do you feel confident you’re invested in the right balance of stocks and
                    bonds?
                  </h4>
                  <p>
                    If not, you’re not alone. When you hire blooom, you put the power of seasoned
                    financial advisors and top financial planning technology on your retirement so
                    you can feel confident it’s working toward reaching your goals.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Balance.svg' />
                </Col>
              </Row>
            </div>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content ' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h4>2. Did you minimize the hidden investment fees in your 401k and IRA?</h4>
                  <p>
                    If you don’t know then let us analyze your account for you. When you hire blooom
                    we look at the expense ratio of every fund and put you in the lowest cost
                    options for your proper allocation.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Fees.svg' />
                </Col>
              </Row>
            </div>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content no-reverse' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h4>3. Is your portfolio based on your retirement horizon and risk profile?</h4>
                  <p>
                    It’s important to have a properly allocated portfolio based on your needs. If
                    you’re too conservative with your investments early in your career, you may be
                    missing out on key years of growth. Unless you’re into working longer and
                    contributing more to hit your goals, making a few good choices early on can make
                    all the difference. But it’s never too late!
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Portfolio.svg' />
                </Col>
              </Row>
            </div>
            <div className='financialscreen__four-content-wrapper'>
              <Row className='financialscreen__content ' gutter={[50, 0]}>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-text'
                  data-aos='fade-down'
                >
                  <h4>
                    4. Do you adjust your investments several times a year to keep you on track??
                  </h4>
                  <p>
                    Keeping up with rebalancing can be a chore. From market conditions to fund
                    offerings, it’s impossible to foresee what may impact your hard-earned savings.
                    When you hire blooom, you can feel confident we’ll rebalance for you based on
                    your time horizon to retirement instead of market hearsay.
                  </p>
                </Col>
                <Col
                  xs={12}
                  className='financialscreen__content__col financialscreen__content__col-image'
                >
                  <img src='/images/Investment-Control.svg' />
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>

      <div className='financialscreen__five-content'>
        <div className='container'>
          <div className='homescreen__five-content-wrapper' data-aos='fade-down'>
            <h2>
              Still have questions? <br />
              We can help.
            </h2>
            <p>Check it out for yourself.</p>
            <NewButton text='Let’s do it' type='primary' />
          </div>
        </div>
      </div>

      <div className='financialscreen__list-card-content'>
        <div className='container'>
          <Row gutter={[50, 0]}>
            <Col className='financialscreen__list-card-content__wrapper'>
              <div data-aos='fade-down'>
                <div className='financialscreen__list-card-content__list-item'>{renderList()}</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </main>
  );
};

export default FinancialPlanningScreen;
