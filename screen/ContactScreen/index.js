import React from "react";
import { Row, Col, Input } from "antd";
import Lottie from "react-lottie";

import ShakeHandLottie from "public/lotties/shake-hand.json";
import NewButton from "components/Common/NewButton";

import "./style.scss";

const ContactScreen = () => {
  return (
    <main className="contact-screen">
      <div className="container">
        <Row justify='center' gutter={[50,0]}>
          <Col className='contact-screen__wrapper'>
            <Row gutter={[50,0]}>
              <Col xs={24} className='contact-screen__col-left'>
                <p className='contact-screen__col-left__top'>REACH OUT TODAY</p>
                <h1>Contact blooom</h1>
                <p className='contact-screen__col-left__text'>
                  Unlike your ex, we are super responsive. Please reach out! Our
                  team would love to hear what you have to say and to answer any
                  questions you might have about blooom
                </p>
                <div className="contact-screen__box-animation">
                  <Lottie
                    options={{
                      loop: true,
                      autoplay: true,
                      animationData: ShakeHandLottie,
                    }}
                  />
                </div>
              </Col>
              <Col xs={24} className='contact-screen__col-right'>
                <Row className='contact-screen__col-right__row'>
                  <Col xs={24} className='contact-screen__col-right__item'>
                    <span>Name * </span>
                    <Input />
                  </Col >
                  <Col xs={24} className='contact-screen__col-right__item'>
                    <span>Email * </span>
                    <Input />
                  </Col>
                  <Col xs={24} className='contact-screen__col-right__item'>
                    <span>Message * </span>
                    <Input.TextArea />
                  </Col>
                </Row>
                <div className='contact-screen__col-right__btnWrapper'>
                <NewButton text='SUBMIT' type='four'/>
                </div>
                
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </main>
  );
};

export default ContactScreen;
