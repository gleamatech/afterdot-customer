import { Col, Row } from 'antd';
import NewButton from 'components/Common/NewButton';
import React from 'react';
import { FaCheck, FaRegCheckCircle, FaRegTimesCircle, FaRulerHorizontal } from 'react-icons/fa';
import './style.scss';
const PricingScreen = () => {
  return (
    <main className='pricingscreen'>
      <section className='pricingscreen-section' data-aos='fade-down'>
        <div className='container'>
          <div className='pricingscreen__banner'>
            <div className='pricingscreen__banner-about'>
              <h1>ONLINE FINANCIAL ADVISOR FEES</h1>
              <h2>
                <span>blooom&nbsp;</span>pricing
              </h2>
              <p>A traditional advisor could cost you thousands of dollars a year.</p>
              <p>Blooom’s pricing is simple.</p>
              <p>
                <span>----</span>
                <br />
                <span>----</span>
              </p>
              <h3>Start for free, then hire blooom if you’d like help!</h3>
            </div>
            <div className='pricingscreen__banner-content'>
              <Row>
                <Col md={8} sm={12} className='pricingscreen__banner-item'>
                  <strong>1. Answer</strong> a few questions about your retirement to get your
                  recommended strategy.s
                </Col>
                <Col md={8} sm={12} className='pricingscreen__banner-item'>
                  <strong>2. Link </strong>an account to see how your actual investments compare to
                  your recommended strategy.
                </Col>
                <Col md={8} sm={12} className='pricingscreen__banner-item'>
                  <strong>3. Make changes </strong>to your account on your own, or hire blooom to do
                  it for you!
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </section>
      <section className='pricingscreen-section' data-aos='fade-down'>
        <div className='container'>
          <Row className='pricingscreen__getstarted'>
            <Col sm={12} className='pricingscreen__getstarted-startfor'>
              <div className='pricingscreen__getstarted-price'>
                <b>Start for</b>
                <br />
                <b>$0</b>
              </div>
              <p>Per year, unlimited Accounts</p>
              <NewButton
                text='Get Started'
                type='primary'
                className='pricingscreen__getstarted-button'
              />
            </Col>
            <Col sm={12} className='pricingscreen__getstarted-desc'>
              <div className='container'>
                <ul>
                  <li>Access to Planning Tools</li>
                  <li>Expert Insights</li>
                  <li>Optimal Strategy Defined</li>
                  <li>Analysis of Current Investments</li>
                  <li>Ideal-to-Actual Comparison</li>
                  <li>Tips for Improvement</li>
                </ul>
              </div>
            </Col>
          </Row>
        </div>
      </section>
      <section className='pricingscreen-section' data-aos='fade-down'>
        <div className='container'>
          <Row>
            <Col md={8} sm={12} className='pricingscreen__options'>
              <div className='pricingscreen__options-type'>
                <div className='pricingscreen__options-price'>
                  <h1>DIY</h1>
                  <h2>$45</h2>
                  <span>per year</span>
                  <p>
                    <FaRegCheckCircle color='' />
                    per Account
                  </p>
                </div>
                <div className='pricingscreen__options-desc'>
                  <ul>
                    <li>
                      <FaRegCheckCircle color='' /> Personalized Portfolio
                    </li>
                    <li>
                      <FaRegTimesCircle /> Placing Trades
                    </li>
                    <li>
                      <FaRegTimesCircle /> Withdrawal Alerts
                    </li>
                    <li>
                      <FaRegTimesCircle /> Advisor Access
                    </li>
                    <li>
                      <FaRegTimesCircle /> Priority Advisor Access
                    </li>
                  </ul>
                </div>
                <NewButton text='Select' type='primary' className='pricingscreen__options-button' />
              </div>
            </Col>
            <Col md={8} sm={12} className='pricingscreen__options'>
              <div className='pricingscreen__options-type'>
                <div className='pricingscreen__options-price'>
                  <h1>Standard</h1>
                  <h2>$120</h2>
                  <span>per year</span>
                  <p>
                    <FaRegCheckCircle color='' />
                    per Account
                  </p>
                </div>
                <div className='pricingscreen__options-desc'>
                  <ul>
                    <li>
                      <FaRegCheckCircle color='' /> Personalized Portfolio
                    </li>
                    <li>
                      <FaRegTimesCircle /> Placing Trades
                    </li>
                    <li>
                      <FaRegTimesCircle /> Withdrawal Alerts
                    </li>
                    <li>
                      <FaRegTimesCircle /> Advisor Access
                    </li>
                    <li>
                      <FaRegTimesCircle /> Priority Advisor Access
                    </li>
                  </ul>
                </div>
                <NewButton text='Select' type='primary' className='pricingscreen__options-button' />
              </div>
            </Col>
            <Col md={8} sm={12} className='pricingscreen__options'>
              <div className='pricingscreen__options-type'>
                <div className='pricingscreen__options-price'>
                  <h1>Unlimited</h1>
                  <h2>$250</h2>
                  <span>per year</span>
                  <p>
                    <FaRegCheckCircle color='' />
                    Unlimited Accounts*
                  </p>
                </div>
                <div className='pricingscreen__options-desc'>
                  <ul>
                    <li>
                      <FaRegCheckCircle color='' /> Personalized Portfolio
                    </li>
                    <li>
                      <FaRegTimesCircle /> Placing Trades
                    </li>
                    <li>
                      <FaRegTimesCircle /> Withdrawal Alerts
                    </li>
                    <li>
                      <FaRegTimesCircle /> Advisor Access
                    </li>
                    <li>
                      <FaRegTimesCircle /> Priority Advisor Access
                    </li>
                  </ul>
                </div>
                <NewButton text='Select' type='primary' className='pricingscreen__options-button' />
              </div>
            </Col>
          </Row>
        </div>
      </section>
      <section className='pricingscreen-section' data-aos='fade-down'>
        <div className='container'>
          <Col className='pricingscreen__table'>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title' >
                <h1> # Accounts Optimized</h1>
                <p>
                  <a>Supported accounts</a>
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">DIY</h3>
                <h6>One</h6>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">Standard</h3>
                <h6>One</h6>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">Unlimited</h3>
                <h6>Unlimited*</h6>
              </Col>
            </Row>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title'>
                <h1>Personalized Portfolio</h1>
                <p>
                  <ul>
                    <li>Research the funds in your plan</li>
                    <li>Pick the optimal funds for your situation</li>
                    <li>Minimized hidden investment fees</li>
                    <li>Keep your portfolio on track as your target allocation shifts</li>
                  </ul>
                </p>
              </Col>

              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>DIY</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Standard</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Unlimited</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
            </Row>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title'>
                <h1>Placing Trades</h1>
                <p>Choose to have blooom place trades on your behalf.</p>
              </Col>

              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>DIY</h3>
                <p>
                  <FaRulerHorizontal />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Standard</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Unlimited</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
            </Row>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title'>
                <h1>Advisor Access</h1>
                <p>
                  Ask an advisor all your financial questions. Receive a response within 2-3
                  business days.
                </p>
              </Col>

              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>DIY</h3>
                <p>
                  <FaRulerHorizontal />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Standard</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Unlimited</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
            </Row>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title'>
                <h1>Priority Advisor Access</h1>
                <p>Live chat with an advisor about all of your financial questions.**</p>
              </Col>

              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>DIY</h3>
                <p>
                  <FaRulerHorizontal />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Standard</h3>
                <p>
                  <FaRulerHorizontal />
                </p>
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3>Unlimited</h3>
                <p>
                  <FaCheck />
                </p>
              </Col>
            </Row>
            <Row className='pricing__table-content'>
              <Col md={12} sm={24} className='pricingscreen__table-title'>
                <h1>Price</h1>
                <p>We never make money off the investments in your account.</p>
              </Col>

              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">DIY</h3>
                <h4 >$45</h4>
                <h5>per year</h5>
                <NewButton text='Select' type='primary' className='pricingscreen__table-button' />
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">Standard</h3>
                <h4>$120</h4>
                <h5>per year</h5>
                <NewButton text='Select' type='primary' className='pricingscreen__table-button' />
              </Col>
              <Col md={4} sm={8} className='pricingscreen__table-element'>
                <h3 className="pricingscreen__table-name">Unlimited</h3>
                <h4>$250</h4>
                <h5>per year</h5>
                <NewButton text='Select' type='primary' className='pricingscreen__table-button' />
              </Col>
            </Row>
          </Col>
        </div>
      </section>
      <section className='pricingscreen-section' data-aos='fade-down'>
        <div className='container'>
          <div className='pricingscreen__summary'>
            <p className='pricingscreen__summary-small'>Choose pricing at checkout.</p>
            <p>
              Blooom may be limited in its analysis of your current investments and only able to
              provide a basic comparison without additional research. In these cases, additional
              research does require a blooom subscription and will require blooom to access your
              account.
            </p>
            <p className='pricingscreen__summary-small'>
              * In order to deliver a personalized retirement plan, all accounts managed in the
              Unlimited plan must be held by the same individual.
            </p>
            <p className='pricingscreen__summary-small'>** During normal business hours.</p>
          </div>
        </div>
      </section>
    </main>
  );
};

export default PricingScreen;
