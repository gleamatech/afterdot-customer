import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';
import useTranslation from 'next-translate/useTranslation';

import NewButton from 'components/Common/NewButton';
import NewCardContent from 'components/Common/NewCardContent';

import images from 'config/images';

import './style.scss';

const HomeScreen = () => {
  const userRedux = useSelector((state) => state.userRedux);
  const { t } = useTranslation();

  const [keyActive, setKeyActive] = useState('');

  const handleClickItem = (number) => () => {
    console.log(number);
    if (number === keyActive) {
      return setKeyActive('');
    }
    return setKeyActive(number);
  };

  const renderList = () => {
    return [1, 2, 3, 4, 5, 6].map((item) => {
      return (
        <NewCardContent
          isOpen={keyActive === item}
          id={item}
          title='When Wall Street lets you down…'
          className='homescreen__list-card-content__item'
          onClick={handleClickItem}
          key={item}
        >
          <p>
            The financial industry is broken. For years, people could rely on pensions and social
            security for retirement, both of which are scarce options today. Many American’s best
            chance at retiring comfortably lies in their 401k, which is (purposely?) confusing,
            forcing us all to fend for ourselves when it comes to understanding and investing in our
            future. For decades, professional financial advice has been reserved only for clients
            with large account balances. Wall Street brokers have preyed off those willing to
            invest, but confused by the plethora of options, leading to distrust and frustration
            with the financial industry as a whole. There’s got to be a better way.
          </p>
          <p>
            <strong>
              Blooom believes that everyone deserves access to quality, unbiased financial advice.
            </strong>{' '}
            That’s why our founders started blooom. Fed up with Wall Street’s inability to help the
            average investor, Randy AufDerHeide, Kevin Conard and Chris Costello set out to create a
            way for everyone to get basic help with their retirement assets, no matter the size of
            their account, or where it’s held. They created a company, and set of tools, that knocks
            down the barriers to entry of a traditional financial planner.
          </p>
        </NewCardContent>
      );
    });
  };

  return (
    <main className='homescreen'>
      <div className='homescreen__first-content'>
        <div className='container'>
          <div className='homescreen__content-wrapper'>
            <Row className='homescreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='homescreen__content__col homescreen__content__col-text'
                data-aos='fade-down'
              >
                <h1>YOUR ONLINE FINANCIAL ADVISOR</h1>
                <h2>Meet blooom.</h2>
                <p>
                  Whether you’re deciding where to begin with your retirement investments or just
                  need a little guidance, blooom is here to help.
                </p>
                <div>
                  <NewButton text='Let’s Get Started!' type='primary' />
                </div>
              </Col>
              <Col xs={12} className='homescreen__content__col homescreen__content__col-image'>
                <img src={images.bannerFacesGrid} />
              </Col>
            </Row>
          </div>
          <div className='homescreen__content-wrapper'>
            <Row className='homescreen__content no-reverse' gutter={[50, 0]}>
              <Col xs={12} className='homescreen__content__col homescreen__content__col-image'>
                <img src={images.bannerCircleFacePlaint} />
              </Col>
              <Col
                xs={12}
                className='homescreen__content__col homescreen__content__col-text'
                data-aos='fade-down'
              >
                <h2>So you have a 401k or IRA…</h2>
                <p>
                  Great! Simply saving is a great first step. But could you be doing better? Hidden
                  investment fees and improper allocation can eat away at retirement savings.
                </p>
              </Col>
            </Row>
          </div>
          <div className='homescreen__content-wrapper'>
            <Row className='homescreen__content' gutter={[50, 0]}>
              <Col
                xs={12}
                className='homescreen__content__col homescreen__content__col-text'
                data-aos='fade-down'
              >
                <h2>Wouldn’t it be great to have an expert’s opinion?</h2>
                <p>
                  We think so. That’s why we created blooom! Simply link your existing investments
                  to your blooom platform for free insight into how you’re doing. See something you
                  need help with? Hire blooom!
                </p>
              </Col>
              <Col xs={12} className='homescreen__content__col homescreen__content__col-image'>
                <img src={images.bannerComputer} />
              </Col>
            </Row>
          </div>
        </div>
      </div>
      <div className='homescreen__second-content'>
        <div className='container'>
          <div className='homescreen__second-content__wrapper' data-aos='fade-down'>
            <img src={images.bannerLineoFaces} alt='' />
            <h3>
              <span>Start with a few simple questions</span>
              <br />
              <span>about you and your investing style…</span>
            </h3>
            <label>My name is *</label>
            <input type='text' placeholder='First name' />
            <NewButton text='NEXT' className='homescreen__second-content__button' />
          </div>
        </div>
      </div>
      <div className='homescreen__third-content'>
        <div className='container'>
          <div data-aos='fade-down'>
            <h2>You might be wondering…</h2>
            <h4>
              <br />
              Am I paying too much in investment fees?
            </h4>
            <NewButton text='Ask blooom' type='primary' />
            <h4>
              <br />
              Can I afford my current lifestyle in retirement?
            </h4>
            <NewButton text='Ask blooom' type='primary' />
            <h4>
              <br />
              What’s the catch here?
            </h4>
            <NewButton text='Ask blooom' type='primary' />
          </div>
        </div>
      </div>
      <div className='homescreen__four-content'>
        <div className='container'>
          <div className='homescreen__four-content__wrapper' data-aos='fade-down'>
            <h2>Start for free, then hire blooom if you need help!</h2>
            <NewButton text='View Pricing' type='primary' />
          </div>
        </div>
      </div>
      <div className='homescreen__list-brand'>
        <div className='container'>
          <Row gutter={[50, 0]} justify='center'>
            <Col className='homescreen__list-brand__wrapper'>
              <div data-aos='fade-down'>
                <h3>As featured in…</h3>
                <img src={images.listBrand} alt='' />
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className='homescreen__list-card-content'>
        <div className='container'>
          <Row gutter={[50, 0]}>
            <Col className='homescreen__list-card-content__wrapper'>
              <div data-aos='fade-down'>
                <p className='homescreen__list-card-content__title'>
                  Want to learn more about <span>blooom</span>?
                  <br />
                  Here’s a bit of light reading.
                </p>
                <div className='homescreen__list-card-content__list-item'>{renderList()}</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </main>
  );
};

export default HomeScreen;
