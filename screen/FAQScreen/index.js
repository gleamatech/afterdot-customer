import React from "react";
import { Input, Row, Col} from "antd";

import ContentList from "./components/ContentList";
import NewButton from "components/Common/NewButton";

import "./style.scss";

const FAQScreen = () => {
  return (
    <main className="faq-screen">
      <div className="container">
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <div className='faq-screen__first-content-wrapper'>
              <h1>We frequently ask questions.<br/> So do our fans.</h1>
              <div className='faq-screen__first-content-wrapper__text'>SEARCH OUR FREQUENTLY ASKED QUESTIONS</div>
              <Row className='faq-screen__first-content-wrapper__row'>
                <Col className='faq-screen__first-content-wrapper__col'>
                  <Input />
                </Col>
                <Col className='faq-screen__first-content-wrapper__col'>
                  <NewButton text='SEARCH' type='four'/>
                </Col>
              </Row>
            </div>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Blooom services</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Pricing</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Account types</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>IRA</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>HSA, 529, Coverdell, etc.</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Investmants</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Deposits, withdrawals, transfers</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Account management</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Linking an account</h2>
          </ContentList>
        </div>
        <div className='faq-screen__block' data-aos='fade-down'>
          <ContentList>
            <h2>Security</h2>
          </ContentList>
        </div>
      </div>
    </main>
  );
};

export default FAQScreen;
