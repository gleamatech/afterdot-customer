import React, { useState } from 'react';

import NewCardContent from 'components/Common/NewCardContent';

import '../style.scss'

const listTest = [1,2,3,4]

const ContentList = ({children}) => {

    const [keyActive, setKeyActive] = useState('')

    const handleClickItem = (value) => () => {
        if(value === keyActive) {
            setKeyActive('')
        } else setKeyActive(value)
    }

    const renderListItem = () => {

        return listTest.map(item => {
            return (
                <NewCardContent
          isOpen={keyActive === item}
          id={item}
          title="When Wall Street lets you down…"
          className="faq-screen__list-content__item"
          onClick={handleClickItem}
          key={item}
        >
          <p>
            The financial industry is broken. For years, people could rely on
            pensions and social security for retirement, both of which are
            scarce options today. Many American’s best chance at retiring
            comfortably lies in their 401k, which is (purposely?) confusing,
            forcing us all to fend for ourselves when it comes to understanding
            and investing in our future. For decades, professional financial
            advice has been reserved only for clients with large account
            balances. Wall Street brokers have preyed off those willing to
            invest, but confused by the plethora of options, leading to distrust
            and frustration with the financial industry as a whole. There’s got
            to be a better way.
          </p>
          <p>
            <strong>
              Blooom believes that everyone deserves access to quality, unbiased
              financial advice.
            </strong>{" "}
            That’s why our founders started blooom. Fed up with Wall Street’s
            inability to help the average investor, Randy AufDerHeide, Kevin
            Conard and Chris Costello set out to create a way for everyone to
            get basic help with their retirement assets, no matter the size of
            their account, or where it’s held. They created a company, and set
            of tools, that knocks down the barriers to entry of a traditional
            financial planner.
          </p>
        </NewCardContent>
            )
        })
    }

    return (
        <div className='faq-screen__content-list'>
            <div className='faq-screen__content-list__top-content'>
                {children}
            </div>
            <div className='faq-screen__content-list__bottom-content'>
                {renderListItem()}
            </div>
        </div>
    )
}

export default ContentList