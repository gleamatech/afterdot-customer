import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { motion } from 'framer-motion';
import NewButton from 'components/Common/NewButton';
import { Turn as Hamburger } from 'hamburger-react';
import { Form, Input, Button, message } from 'antd';
import { FaFacebook, FaApple } from 'react-icons/fa';
import Link from 'next/link';
import { BaseAPI } from 'config/BaseAPI';
import { setUserRedux } from 'controller/redux/action/storageActions';
import { setItemStorage } from 'common/functions';
import { useRouter } from 'next/router';
import { KEY_STORE } from 'common/constans';
import { googleProvider, facebookProvider } from 'config/firebase/authMethod';
import socialMediaAuth from 'common/service';

import './style.scss';
import { setMessageAuth } from 'controller/redux/action/pageActions';

const URL_LOGIN = '/api/users/login';

const LogInScreen = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isFocus, setIsFocus] = useState(false)
  const [isFocusEmail, setIsFocusEmail] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const authPage = useSelector((state) => state.pageRedux.authPage);

  const onFinish = async (values) => {
    setIsLoading(true);
    try {
      const { email, password } = values;
      const response = await BaseAPI.post(URL_LOGIN, { email, password });
      if (response == undefined) {
        setError('The user is not exists or no longer exists on our system');
        setError(null);
      }
      const { token, user } = response;
      setItemStorage(KEY_STORE.JWT_TOKEN, token);
      dispatch(setUserRedux({ token, user }));
      setIsLoading(false);
      router.push('/');
    } catch (error) {
      setIsLoading(false);
    }
  };

  const handleChangeEmail = (e) => {
    const { value } = e.target
    setEmail(value)
  }

  const handleChangePass = (e) => {
    const { value } = e.target
    setPassword(value)
  }

  const variants = {
    close: {
      opacity: 1,
      y: -50,
      transition: {
        duration: 0.5
      }
    },
    open: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1.5
      },
      transitionEnd: {
        display: 'none'
      }
    }
  };

  
  const onClickSocial = (provider) => async () => {
    const res = await socialMediaAuth(provider);
    if (res.status) {
      const user = res.data.getIdTokenResult().then(async (token) => {
        setIsLoading(true);
        const response = await BaseAPI.post("/api/users/loginWithSocial", {
          token: token.token,
        });
        if (response.status) {
          const { token, user } = response;
          setItemStorage(KEY_STORE.JWT_TOKEN, token);
          dispatch(setUserRedux({ token, user }));
          setIsLoading(false);
          router.push("/");
        }
      });
    }
  };

  const handleFocus = (type = 'email', value) => () => {
    if(type === 'email') {
      if(email){
        setIsFocusEmail(true)
      }else setIsFocusEmail(value)
    } else {
      if(password){
        setIsFocus(true)
      }else {
        setIsFocus(value)
      }
    }
  }

  useEffect(() => {
    dispatch(setMessageAuth({ actionsLoading: false }));
  }, []);

  return (
    <main className='login-screen'>
      <div className='container'>
        <div className='login-screen__wrapper'>
          {authPage.actionsLoading && message.success(`${authPage.message.success}`, 2.5)}

          {error && message.error(`${error}`, 2.5)}

          <div className='login-screen__form'>
            <h3>Welcome Back</h3>
            <p>
              Enter your email address and password <br />
              below to login to your account.
            </p>
            <Form name='basic' onFinish={onFinish} initialValues={{ remember: true }}>
              <Form.Item
                name='email'
                label='Email address'
                className={`form-item ${isFocusEmail && 'form-item--isFocusEmail'}`}
                rules={[
                  { required: true, message: 'Required' },
                  { type: 'email', message: 'Must be a valid email address' }
                ]}
              >
                <Input value={email} onFocus={handleFocus('email',true)} onBlur={handleFocus('email',false)} onChange={handleChangeEmail}/>
              </Form.Item>

              <Form.Item
                name='password'
                label='Password'
                className={`form-item form-item-password ${isFocus && 'form-item--isFocus'}`}
                rules={[
                  { required: true, message: 'Required' },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (getFieldValue('password').length > 6) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Minimum length is 6 characters.'));
                    }
                  })
                ]}
              >
                <Input.Password onFocus={handleFocus('password',true)} onBlur={handleFocus('password',false)} value={password} onChange={handleChangePass}/>
              </Form.Item>
              <Form.Item className='mb-0'>
                <Button loading={isLoading} htmlType='submit'>
                  Sign In
                </Button>
              </Form.Item>
            </Form>
          </div>
          <div className='login-screen__line'>
            <div className='line'></div>
            <div className='text'>or</div>
          </div>
          <div className='login-screen__options'>
            <div
              className='login-screen__options-item option-google'
              onClick={onClickSocial(googleProvider)}
            >
              <img src='/images/google-logo.png' alt='' />
              <p>Sign in with Google</p>
              <span></span>
            </div>
            {/* <div
              className='login-screen__options-item option-facebook'
              onClick={onClickSocial(facebookProvider)}
            >
              <FaFacebook />
              <p>Sign in with Facebook</p>
              <span></span>
            </div> */}
          </div>
          <div className='login-screen__direction'>
            <Link href='/signup/introduction/1' to='/signup/introduction/1'>
              <p>Sign up</p>
            </Link>
            <span></span>
            <Link href='/forgot-password' to='/forgot-password'>
              <p>Forgot password</p>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
};

export default LogInScreen;
