import React from 'react';
import './style.scss';
const NotFoundScreen = () => {
  return (
    <main className='notfoundscreen'>
      <div className='container'>
        <div className='notfoundscreen__title'>Not Found</div>
        <div className='notfoundscreen__content'>
          <div>Sorry, but the page you were trying to view does not exist.</div>
          <p>It looks like this was the result of either:</p>
          <ul>
            <li>a mistyped address</li>
            <li>an out-of-date link</li>
          </ul>
        </div>
      </div>
    </main>
  );
};

export default NotFoundScreen;
