import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyBZ8nSHCnP69fLJKswwNWHNRrF2IC3U29M",
    authDomain: "afterdot-46fda.firebaseapp.com",
    projectId: "afterdot-46fda",
    storageBucket: "afterdot-46fda.appspot.com",
    messagingSenderId: "84008948880",
    appId: "1:84008948880:web:e60ad65ca7888425c9b415"
  };

// const firebaseClient = firebase.initializeApp(firebaseConfig)

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}else {
  firebase.app(); // if already initialized, use that one
}

export default firebase