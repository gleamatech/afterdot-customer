const images = {
  //banner
  bannerFacesGrid: require('config/images/banner/FacesGrid.png'),
  bannerCircleFacePlaint: require('config/images/banner/CircleFacePlant.png'),
  bannerComputer: require('config/images/banner/banner-computer.png'),
  bannerLineoFaces: require('config/images/banner/LineoFaces.png'),
  listBrand: require('config/images/banner/Mags_horizontal.png'),

  //logo
  logo: require('config/images/logo/logo.svg')
};

export default images;
