import axios from "axios";
import { getItemStorage } from "common/functions";

export const BaseAPI = axios.create({
  baseURL: process.env.NEXT_PUBLIC_APP_API,
  timeout: 12000
});

BaseAPI.interceptors.response.use(
  (config) => {
    const status = config.data;
    if (status) {
      return config.data;
    }
    return config;
  },
  (error) => {
    console.log("error", error);
  }
);

BaseAPI.interceptors.response.use(
  (config) => {
    if (status) {
      return config.data;
    }
    return config;
  },
  (error) => {
    console.log("error", error);
  }
);

BaseAPI.interceptors.request.use((config) => {
  const token = getItemStorage("JWT_TOKEN");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});
