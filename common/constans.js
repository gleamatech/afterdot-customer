export const REQUEST_TYPE = {
    POST: 'post',
    GET: 'get',
    PUT: 'put',
    DELETE: 'delete',
    PATCH: 'patch'
  }

  export const KEY_STORE = {
    JWT_TOKEN: 'JWT_TOKEN',
  }