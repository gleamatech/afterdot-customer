import firebase from 'config/firebase/firebaseConfig'
const socialMediaAuth = (provider) => {
    return firebase.auth().signInWithPopup(provider).then((res) =>{
        return {
            status : true,
            data: res.user
        }
    }).catch((error) => {
        return {
            status: false,
            data: error
        }
    })
}

export default socialMediaAuth