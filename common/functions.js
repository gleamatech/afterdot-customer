export const getItemStorage = (key) => JSON.parse(localStorage.getItem(key));

export const setItemStorage = async (key, value) => {
  await localStorage.setItem(key, JSON.stringify(value));
};

export const removeItemStorage = (key) => localStorage.removeItem(key);
