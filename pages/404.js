import React from 'react';
import NotFoundScreen from 'screen/NotFoundScreen';

export default function PageNotFound() {
  return <NotFoundScreen />;
}
