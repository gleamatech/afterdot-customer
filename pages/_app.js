import React, { useEffect, useState } from 'react';
import Layout from 'containers/Layout';
import AOS from 'aos';
import { PersistGate } from 'redux-persist/integration/react';
import { useDispatch } from 'react-redux';
import { ReduxWrapper } from 'controller/redux/store/configureStore';

import 'aos/dist/aos.css';
import '../styles/antd-custom.less';
import 'styles/global.scss';
import { getItemStorage } from 'common/functions';
import { BaseAPI } from 'config/BaseAPI';
import { setUserRedux } from 'controller/redux/action/storageActions';
import { useRouter } from 'next/router';

const URL_GET_ME = '/api/users/getme';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const dispatch = useDispatch();
  const [token, setToken] = useState(null);
  if (token) {
    (async () => {
      try {
        const response = await BaseAPI.get(URL_GET_ME);
        if (!response.status) return;
        dispatch(setUserRedux(response.user));
      } catch (error) {}
    })();
  }
  useEffect(() => {
    setToken(getItemStorage('JWT_TOKEN'));
    if (token) {
      switch (router.pathname) {
        case router.pathname === '/login':
          router.push('/');
          break;

        default:
          break;
      }
    }
  }, [token, router.pathname]);

  useEffect(() => {
    AOS.init({
      duration: 1000,
      once: true
    });
    AOS.refresh();
  }, []);

  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default ReduxWrapper.withRedux(MyApp);
