import React from 'react';
import IntroductionScreen from 'screen/SignUpScreen/IntroductionScreen';

const IntroductionStep = () => {
  return <IntroductionScreen />;
};

export default IntroductionStep;
