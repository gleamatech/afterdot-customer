import React from 'react';
import BestAdvisorScreen from 'screen/AboutScreen/BestRoboAdvisor';

const BestRoboAdvisor = () => {
  return <BestAdvisorScreen />;
};

export default BestRoboAdvisor;
