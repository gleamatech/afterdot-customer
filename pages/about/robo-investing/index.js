import React from 'react';
import NewButton from 'components/Common/NewButton';
import { Row, Col } from 'antd';
import HandHoldPhone from 'public/lotties/hand-hold-a-phone.json';
import PiggyScroller from 'public/lotties/piggy-scroller.json';
import RoboInvestingScreen from 'screen/AboutScreen/RoboInvesting';

const RoboInvesting = () => {
  return <RoboInvestingScreen />;
};

export default RoboInvesting;
