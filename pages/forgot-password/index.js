import React from 'react';
import ForgotPasswordScreen from 'screen/ForgotPasswordScreen';

const ForgotPassword = () => {
  return <ForgotPasswordScreen />;
};

export default ForgotPassword;
