import { REQUEST_TYPE } from 'common/constants'
import axios from 'axios'

export default class BaseAPI {
  static async getData (type, queryBody) {
    return this.postGateWay(type, REQUEST_TYPE.GET, undefined, queryBody)
  }

  static async postData (type, body) {
    return this.postGateWay(type, REQUEST_TYPE.POST, body)
  }

  static async putData (type, body) {
    return this.postGateWay(type, REQUEST_TYPE.PUT, body)
  }

  static async deleteData (type, queryBody) {
    return this.postGateWay(type, REQUEST_TYPE.DELETE, undefined, queryBody)
  }

  static async postGateWay (action, method = REQUEST_TYPE.GET, body, queryBody) {
    try {
      const serverUrl = process.env.REACT_APP_API
      console.log(serverUrl)
      const config = {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          os: 'website',
        }
      }

      const axiosInstance = axios.create(config)
      let queryStr = ''
      if (queryBody) {
        const queryFly = QueryString.stringify(queryBody)
        queryStr = '?' + queryFly
      }

      const response = await axiosInstance[method](serverUrl + action + queryStr, body, config)
      if (response.status === 200) {
        return response.data
      } else {
        return null
      }
    } catch (error) {
      console.log(error)
      return null
    }
  }
}
