import { createReducer } from '@reduxjs/toolkit';
import { KEY_PAGE } from 'controller/redux/lib/constants';
import { initialState } from '../lib/initState';

export const pageRedux = createReducer(initialState, {
  [KEY_PAGE.SET_BLOG_DETAIL](state, action) {
    return void (state.blogPage.blogDetails = action.payload);
  },
  [KEY_PAGE.SET_BLOG_BY_TAG](state, action) {
    return void (state.blogPage.blogByTag = action.payload);
  },
  [KEY_PAGE.SET_INTRODUCTION_ACTIVE](state, action) {
    return void (state.signUp.activeIntroduction = action.payload);
  },
  [KEY_PAGE.SET_MESSAGE](state, action) {
    return void ((state.authPage.actionsLoading = action.payload.actionsLoading),
    (state.authPage.message.success = action.payload.success),
    (state.authPage.message.error = action.payload.error));
  }
});
