import { createReducer } from '@reduxjs/toolkit';
import { KEY_STORAGE } from 'controller/redux/lib/constants';

export const userRedux = createReducer(
  {},
  {
    [KEY_STORAGE.SET_USER](state, action) {
      return action.payload;
    }
  }
);
