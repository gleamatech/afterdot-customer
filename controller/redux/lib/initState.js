export const initialState = {
  blogPage: {
    blogDetails: null,
    blogByTag: null
  },
  authPage: {
    actionsLoading: false,
    message: {
      success: null,
      error: null
    }
  },
  signUp: {
    activeIntroduction: false
  }
};

export const callTypes = {
  list: 'list',
  action: 'action'
};
