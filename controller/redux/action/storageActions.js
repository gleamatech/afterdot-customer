import { createAction } from '@reduxjs/toolkit'
import { KEY_STORAGE } from 'controller/redux/lib/constants'

export const setUserRedux = createAction(KEY_STORAGE.SET_USER)
