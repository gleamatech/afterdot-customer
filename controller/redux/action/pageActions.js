import { createAction } from '@reduxjs/toolkit';
import { KEY_PAGE } from 'controller/redux/lib/constants';

export const setBlogDetail = createAction(KEY_PAGE.SET_BLOG_DETAIL);
export const setBlogByTag = createAction(KEY_PAGE.SET_BLOG_BY_TAG);
export const setIntroductionActive = createAction(KEY_PAGE.SET_INTRODUCTION_ACTIVE);
export const setMessageAuth = createAction(KEY_PAGE.SET_MESSAGE);
