import { createWrapper } from "next-redux-wrapper";
import { applyMiddleware, createStore, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "../reducers";

const middleware = [thunkMiddleware];

const composeEnhancers = typeof window !== "undefined" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const makeConfiguredStore = (reducer, isServer = true) =>
  createStore(
    rootReducer,
    undefined,
    !isServer
      ? composeEnhancers(applyMiddleware(...middleware))
      : applyMiddleware(...middleware)
  );

const makeStore = () => {
  const isServer = typeof window === "undefined";

  if (isServer) {
    return makeConfiguredStore(rootReducer);
  } else {
    // we need it only on client side
    const { persistStore, persistReducer } = require("redux-persist");
    const storage = require("redux-persist/lib/storage").default;

    const persistConfig = {
      key: "nextjs",
      // whitelist: ['fromClient'], // make sure it does not clash with server keys
      storage,
    };

    const persistedReducer = persistReducer(persistConfig, rootReducer);
    const store = makeConfiguredStore(persistedReducer, isServer);

    store.__persistor = persistStore(store); // Nasty hack

    return store;
  }
};

export const ReduxWrapper = createWrapper(makeStore);
