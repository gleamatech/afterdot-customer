import React from 'react';
import { Button } from 'antd';

import './style.scss';

const NewButton = ({ onClick, onSubmit, text, loading, className, type, disabled }) => {
  const currentType = `my-button-${type || 'normal'}`;
  return (
    <Button
      onClick={onClick}
      onSubmit={onSubmit}
      disabled={disabled}
      loading={loading}
      className={`my-button ${currentType} ${className}`}
      htmlType='submit'
    >
      {text}
    </Button>
  );
};

export default NewButton;
