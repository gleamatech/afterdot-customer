import React from 'react';
import { Select } from 'antd';
import './style.scss';

const NewSelected = ({ listItem, onChange, className }) => {
  const { Option } = Select;
  return (
    <Select
      placeholder='Select Risk Tolerance'
      style={{ width: 240 }}
      className={`selected-custom ${className}`}
      onChange={onChange}
    >
      {listItem.map((item, index) => (
        <Option key={index} value={item.value}>
          {item.name}
        </Option>
      ))}
    </Select>
  );
};

export default NewSelected;
