import React from 'react';
import { Checkbox, Row, Col } from 'antd';

import './style.scss';

const NewCheckBoxGroup = ({ listItem, defaultValue, onChange }) => {
  return (
    <Checkbox.Group style={{ width: '100%' }} defaultValue={defaultValue} onChange={onChange}>
      <Row>
        {listItem.map((item, index) => (
          <Col span={24}>
            <Checkbox value={item.value}>{item.name}</Checkbox>
          </Col>
        ))}
      </Row>
    </Checkbox.Group>
  );
};

export default NewCheckBoxGroup;
