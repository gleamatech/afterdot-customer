import React from 'react';
import { Radio } from 'antd';
import './style.scss';

const RadioButtonGroup = ({ listItem, className, defaultValue, onChange }) => {
  return (
    <Radio.Group
      buttonStyle='solid'
      className={className}
      defaultValue={defaultValue}
      onChange={onChange}
    >
      {listItem.map((item, index) => (
        <Radio.Button key={index} value={item.value}>
          {item.name}
        </Radio.Button>
      ))}
    </Radio.Group>
  );
};

export default RadioButtonGroup;
