import React, { useState } from 'react';
import { motion } from 'framer-motion'
import { AiOutlinePlus } from 'react-icons/ai'

import './style.scss'

const NewCardContent = ({
    children,
    title,
    className,
    onClick,
    isOpen,
    id
}) => {

    const itemAnimate = {
        open: {
          opacity: 1,
          height: 'auto',
          transition:{
            duration: 0.2
          },
          display: "block"
        },
        close: {
          opacity: 0,
          height: 0,
          transition: {
            duration: 0.3
          },
          transitionEnd: {
            display: 'none'
          }
        }
      }

    return (
        <div className={`new-card-content__wrapper ${className}`}>
            <div className='new-card-content__title' onClick={onClick(id)}>
                <h3>
                {title}
                </h3>
                <AiOutlinePlus className={`new-card-content__icon ${isOpen ?'new-card-content__icon--active': null }`}/>
            </div>
            <motion.div 
            initial="close"
            animate={!isOpen ? 'close' : 'open'}
            variants={itemAnimate}>
                <div className='new-card-content__content'>
                    {children}
                </div>
            </motion.div>
        </div>
    )
}

export default NewCardContent