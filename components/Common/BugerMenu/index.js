import React from "react";

import "./style.scss";

const BugerMenu = () => {
  return (
    <div className="my-buger-menu">
      <button class="hamburger hamburger--collapse is-active" type="button">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    </div>
  );
};

export default BugerMenu;
